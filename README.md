# max.model.ledger.abstract_ledger

This repository contains an abstract model that represents a Distributed Ledger 
(which is not necessarily a Blockchain but can also be e.g., a DAG-based ledger).

It constitutes a different alternative 
to the [max.model.ledger.blockchain](https://gitlab.com/cea-licia/max/models/ledgers/max.model.ledger.blockchain)
model.



## Challenges

There were several problems with [max.model.ledger.blockchain](https://gitlab.com/cea-licia/max/models/ledgers/max.model.ledger.blockchain):
- it is specifically related to Blockchains and contains many notions that are not generic 
(blocks, leaders, leader selection, wallet, fees, rewards etc, see e.g. content of 
"[BlockchainContext](https://gitlab.com/cea-licia/max/models/ledgers/max.model.ledger.blockchain/-/blob/main/src/main/java/max/model/ledger/blockchain/env/BlockchainContext.java)")
and we wanted to provide a more generic and simpler abstraction layer.
- it makes use of a
"[BlockCreationOracle](https://gitlab.com/cea-licia/max/models/ledgers/max.model.ledger.blockchain/-/blob/main/src/main/java/max/model/ledger/blockchain/action/BlockCreationOracle.java)"
for the definition of all behaviors related to the consensus mechanism.
And the behavior of all the nodes is dictated by the use of this Oracle.
See e.g. this [TendermintOracle](https://gitlab.com/cea-licia/max/models/ledgers/max.model.ledger.tendermint_v2/-/blob/main/src/main/java/max/model/ledger/tendermint_v2/env/TendermintOracle.java)
which answers questions such as whether or not a block proposal is valid or who is the current proposer for Tendermint.
This is problematic if we want to include Byzantine nodes with specific predetermined unexpected behavior (e.g. if we want to simulate attacks).
The behavior of each node should be dictated by its own local specification and current local state rather than by a global Oracle.


## Principle

With this new model, we introduce a very simple abstraction layer over [max.model.network.stochastic_adversarial_p2p](https://gitlab.com/cea-licia/max/models/networks/max.model.network.stochastic_adversarial_p2p)
that:
- is defined up to a generic transaction type "T_tx"
- characterizes validators that have:
  - a local ledger state which implements an interface "AbstractLocalLedgerState<T_tx>"
  - a locally defined approval policy that implements an interface "AbstractTransactionApprovalPolicy<T_tx>"
  - a local mempool of type "ValidatorLocalClientTransactionsMempool<T_tx>"

Our model provides actions:
- "AcReceiveTransactionToPutInMempool" to receive transactions from clients and via gossip
- "AcDeliverBlockOrWaveOfTransactions" to deliver transactions (i.e. once a consensus instance is resolved some transactions are finalized/delivered in a specific order)

It also provides methods (accessible from the validator's context) to:
- retrieve delivered transactions
- extract from the mempool not yet delivered transactions so that proposers may create new block proposals

Everything is done locally, at the level of each validator.
For instance, validators have a locally defined transactions approval policy which dictates whether or not they deem a transaction valid. 
This is done locally (there are no global oracle to dermine transactions validity).
We provide it as an interface "AbstractTransactionApprovalPolicy<T_tx>" and also a basic accepting policy "AcceptingTransactionApprovalPolicy<T_tx>" which always deems transactions valid.

There is no such thing as a "Blockchain" or any global ledger state.
There are only local ledger states that:
- are intended to converge towards the same state
- are intended to be, at any given time, prefixes of each other (forks being unwanted)

In this model, we provide an interface to manipulate these local ledger states on each validator.
This allows us e.g. to check the respect of properties on the ledger.
For instance, this present abstract ledger model includes a "AcCheckCoherence" action (performed on the environment) 
which verifies that there is no divergence between the local ledger states of each validator.

More specific use cases can provide concrete implementations of local ledger states and of properties to verify.




## Puzzle Mockup/Fixture usecase

We also provide a simple use case for an application layer that runs on top of the distributed ledger (i.e. of what the distributed ledger do).
This will then allow us to compare different distributed ledger implementations of the exact same application.

This use case corresponds to a competition/a game between clients that attempt to solve successive puzzles.
When a client solves a puzzle, it sends a transaction with the solution to one/some/all the validators of the ledger.
For any puzzle, the first client that has a transaction with its solution delivered in the ledger wins the game.
This game is repeated over successive puzzle.

This usecase corresponds to:
- providing a concrete type for transactions "T_tx" via "PuzzleMockupTransaction"
- providing a concrete type implementing the interface "AbstractLocalLedgerState<PuzzleMockupTransaction>" via "PuzzleMockupLocalLedgerState"

We also provide a transaction approval policy that ostracizes a specific client i.e., never accept puzzle solutions from it.
This "PuzzleMockupOstracizingApprovalPolicy" implements "AbstractTransactionApprovalPolicy<PuzzleMockupTransaction>"
and can be used to simulate systems in which an adversary attempts to violate the game's fairness towards a specific client.

All clients having the same aptitude, the game is client-fair iff all clients have the same likelihood of winning.
Other fairness properties, in particular order-fairness properties can be verified using this use case.
These properties correspond to comparing the order with which transactions are initially received by validators 
from clients and the order with which they are delivered in the ledger.
We provide an "AcCheckOrderFairnessForPuzzles" action (performed on the environment)
which verifies these properties by analyzing the local ledger states of each validator.

The "PuzzleMockupTransaction" data-type also allows the definition of "heartbeat" transactions that do not represent a client sending a puzzle solution.
These "heartbeat" transactions allow feeding the ledger to keep it live while having no direct effect on the puzzle competition.



# Licence

__max.model.ledger.abstract_ledger__ is released under
the [Eclipse Public Licence 2.0 (EPL 2.0)](https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html)













