/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.action.check;


import madkit.kernel.AgentAddress;
import max.core.action.Action;
import max.core.role.Role;
import max.model.ledger.abstract_ledger.role.RAbstractLedgerChannel;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;

import max.model.ledger.abstract_ledger.env.LedgerValidatorContext;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.network.stochastic_adversarial_p2p.env.StochasticP2PEnvironment;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Action executed by the environment in which the ledger validators exist.
 * Its aim is to verify the consistency of the ledger
 * i.e. whether or not all the local copies of the ledger (ordered list of blocks of transactions)
 * are prefixes of each other (some validators might be late w.r.t. other but no forks are allowed)
 *
 * @author Erwan Mahe
 */
public class AcCheckCoherence<T_tx, T_st extends AbstractLocalLedgerState<T_tx>> extends Action<StochasticP2PEnvironment> {


    /**
     * The validatorRole so that we are able to identify validators in the ledger environment.
     * **/
    private final Class<? extends Role> validatorRole;


    public AcCheckCoherence(String environment,
                            StochasticP2PEnvironment owner,
                            Class<? extends Role> validatorRole) {
        super(environment, RAbstractLedgerChannel.class, owner);
        this.validatorRole = validatorRole;
    }

    @Override
    public void execute() {
        List<Pair<String,List<List<T_tx>>>> allLocalCopies = new ArrayList<>();
        Integer minimal_common = null;
        for (AgentAddress address : this.getOwner().getAgentsWithRole(this.getEnvironment(), this.validatorRole)) {
            StochasticPeerAgent agent = (StochasticPeerAgent) address.getAgent();
            LedgerValidatorContext<T_tx,T_st> context = (LedgerValidatorContext<T_tx,T_st>) agent.getContext(this.getEnvironment());
            // ***
            List<List<T_tx>> blocks = context.ledgerValidatorState.getOrderedBlocks();
            getOwner().getLogger().info("validator " + agent.getName() + " has " + blocks.size() + " delivered blocks/vertices");
            if (minimal_common == null) {
                minimal_common = blocks.size();
            } else {
                minimal_common = Math.min(minimal_common, blocks.size());
            }
            allLocalCopies.add(Pair.of(agent.getName(),blocks));
        }
        getOwner().getLogger().info("all validators have delivered at least " + minimal_common + "  blocks/vertices");

        boolean coherent = true;
        iter_pairs_outer : for (int i = 0; i < allLocalCopies.size(); i++) {
            String agent_i = allLocalCopies.get(i).getLeft();
            List<List<T_tx>> localCopy_i = allLocalCopies.get(i).getRight();
            for (int j = i + 1; j < allLocalCopies.size(); j++) {
                String agent_j = allLocalCopies.get(j).getLeft();
                List<List<T_tx>> localCopy_j = allLocalCopies.get(j).getRight();
                int common = Math.min(localCopy_i.size(), localCopy_j.size());
                for (int index = 0; index < common; index++) {
                    List<T_tx> left = localCopy_i.get(index);
                    List<T_tx> right = localCopy_j.get(index);
                    if (!left.equals(right)) {
                        String leftString = left.stream().map(Object::toString)
                                .collect(Collectors.joining(", "));
                        String rightString = right.stream().map(Object::toString)
                                .collect(Collectors.joining(", "));
                        getOwner().getLogger().warning(
                                "block/wave number " + index +
                                        " of agents " + agent_i + " and " + agent_j +
                                        " are different : \n" + leftString + "\n" + rightString
                        );
                        coherent = false;
                        break iter_pairs_outer;
                    }
                }
            }
        }

        if (!coherent) {
            getOwner().getLogger().warning("ledger is not coherent");
        } else {
            getOwner().getLogger().info("coherence OK checked : no divergence");
        }
    }

    @Override
    public <T extends Action<StochasticP2PEnvironment>> T copy() {
        return (T) new AcCheckCoherence(
                getEnvironment(),
                getOwner(),
                this.validatorRole);
    }

}
