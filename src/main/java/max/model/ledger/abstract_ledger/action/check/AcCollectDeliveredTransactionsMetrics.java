/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.action.check;


import madkit.kernel.AgentAddress;
import max.core.action.Action;
import max.core.role.Role;
import max.model.ledger.abstract_ledger.env.LedgerValidatorContext;
import max.model.ledger.abstract_ledger.role.RAbstractLedgerChannel;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import max.model.network.stochastic_adversarial_p2p.env.StochasticP2PEnvironment;
import org.apache.commons.lang3.tuple.Pair;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;


/**
 * Action executed by the environment in which the ledger validators exist.
 * Its aim is to collect some metrics on the ledger which includes:
 * - the number of validators
 * - several numbers of blocks (in blockchains) and waves (in DAG-based ledgers) corresponding to:
 *   + the maximum number of delivered blocks/waves in any of the validators
 *   + the median number of delivered blocks/waves across all validators
 *   + the minimum number of delivered blocks/waves across all validators
 *   + and also the first and third quartiles
 * - and w.r.t. the local copy of the blockchain in the validator with the most blocks/waves:
 *  + the number of duplicated transactions
 *  + and the distribution of the number of transactions within blocks with:
 *    * the minimal block size
 *    * the median block size
 *    * the maximal block size
 *    * and also the first and third quartiles
 *
 * @author Erwan Mahe
 */
public class AcCollectDeliveredTransactionsMetrics<T_tx, T_st extends AbstractLocalLedgerState<T_tx>> extends Action<StochasticP2PEnvironment> {


    private Optional<String> outputCsvPath;

    /**
     * The validatorRole so that we are able to identify validators in the ledger environment.
     * **/
    private final Class<? extends Role> validatorRole;


    public AcCollectDeliveredTransactionsMetrics(String environment,
                                                 StochasticP2PEnvironment owner,
                                                 Class<? extends Role> validatorRole,
                                                 Optional<String> outputCsvPath) {
        super(environment, RAbstractLedgerChannel.class, owner);
        this.validatorRole = validatorRole;
        this.outputCsvPath = outputCsvPath;
    }

    private Pair<T_st,List<Integer>> getReferenceLocalLedgerState() {
        T_st referenceLocalLedgerState = null;
        Integer maxNumBlocks = 0;
        ArrayList<Integer> allNumBlocks = new ArrayList<>();
        String referenceAgentName = null;
        for (AgentAddress address : this.getOwner().getAgentsWithRole(this.getEnvironment(), this.validatorRole)) {
            StochasticPeerAgent agent = (StochasticPeerAgent) address.getAgent();
            LedgerValidatorContext<T_tx,T_st> context = (LedgerValidatorContext<T_tx,T_st>) agent.getContext(this.getEnvironment());
            // ***
            Integer currentValidatorNumBlock = context.ledgerValidatorState.getOrderedBlocks().size();
            allNumBlocks.add(currentValidatorNumBlock);
            if (referenceAgentName == null) {
                referenceLocalLedgerState = context.ledgerValidatorState;
                referenceAgentName = agent.getName();
                maxNumBlocks = currentValidatorNumBlock;
            } else {
                if (currentValidatorNumBlock > maxNumBlocks) {
                    referenceLocalLedgerState = context.ledgerValidatorState;
                    referenceAgentName = agent.getName();
                    maxNumBlocks = currentValidatorNumBlock;
                }
            }
        }
        Integer numValidators = allNumBlocks.size();
        if(numValidators < 4) {
            throw new RuntimeException("there must be at least 4 distinct validators to collect relevant metrics");
        }
        this.getOwner().getLogger().info(
                "there are " + numValidators +
                        " distinct validators, each with their own local copy of the ledger"
        );
        allNumBlocks.sort(Integer::compareTo);
        Integer minimalNumBlocks = allNumBlocks.get(0);
        Integer firstQuartileNumbBlocks = allNumBlocks.get((numValidators/4)-1);
        Integer medianNumBlocks = allNumBlocks.get((numValidators/2)-1);
        Integer thirdQuartileNumbBlocks = allNumBlocks.get(((3*numValidators)/4)-1);

        // The JVM is caching Integer values.
        // Hence the comparison with == only works for numbers between -128 and 127.
        assert(allNumBlocks.get(numValidators -1).equals(maxNumBlocks));

        this.getOwner().getLogger().info(
                "every validator has at least " + minimalNumBlocks +
                        " blocks/waves ; 25% of validators have at least " + firstQuartileNumbBlocks +
                        " blocks/waves ; 50% of validators have at least " + medianNumBlocks +
                        " blocks/waves ; 75% of validators have at least " + thirdQuartileNumbBlocks +
                        " blocks/waves ; the maximum number of blocks/wave any validator has is " + maxNumBlocks
        );
        this.getOwner().getLogger().info(
                "validator " + referenceAgentName +
                        " has the most delivered blocks/waves which is " + maxNumBlocks +
                        "\n we will use it as a reference to check other metrics"
        );
        // TODO: make it clearer, use a custom type instead of a list
        ArrayList<Integer> distributionMetrics = new ArrayList<>();
        distributionMetrics.add(minimalNumBlocks);
        distributionMetrics.add(firstQuartileNumbBlocks);
        distributionMetrics.add(medianNumBlocks);
        distributionMetrics.add(thirdQuartileNumbBlocks);
        distributionMetrics.add(maxNumBlocks);
        return Pair.of(referenceLocalLedgerState,distributionMetrics);
    }

    @Override
    public void execute() {
        Pair<T_st,List<Integer>> pair = this.getReferenceLocalLedgerState();
        T_st referenceLocalLedgerState = pair.getLeft();
        List<Integer> blocksDistributionMetricsAcrossValidators = pair.getRight();
        // ***
        HashMap<T_tx,Pair<Integer, ArrayList<Integer>>> transactionsCount = new HashMap<>();
        Integer numBlocks = referenceLocalLedgerState.getOrderedBlocks().size();
        ArrayList<Integer> numberOfTransactionsPerBlock = new ArrayList<>();
        for (int blockNum = 1; blockNum <= numBlocks; blockNum ++) {
            List<T_tx> block = referenceLocalLedgerState.getOrderedBlocks().get(blockNum - 1);
            numberOfTransactionsPerBlock.add(block.size());
            for (T_tx tx : block) {
                if (transactionsCount.containsKey(tx)) {
                    Pair<Integer, ArrayList<Integer>> got = transactionsCount.get(tx);
                    ArrayList<Integer> inBlocks = got.getRight();
                    inBlocks.add(blockNum);
                    Integer txIsDuplicatedXTimes = got.getLeft();
                    txIsDuplicatedXTimes += 1;
                    transactionsCount.put(tx,Pair.of(txIsDuplicatedXTimes,inBlocks));
                } else {
                    ArrayList<Integer> inBlocks = new ArrayList<>();
                    inBlocks.add(blockNum);
                    transactionsCount.put(tx, Pair.of(1,inBlocks));
                }
            }
        }
        int duplicatedCount = 0;
        for (T_tx tx : transactionsCount.keySet()) {
            int count = transactionsCount.get(tx).getLeft();
            if (count >= 2) {
                ArrayList<Integer> inBlocks = transactionsCount.get(tx).getRight();
                this.getOwner().getLogger().info(
                        "transaction :" + tx +
                                "\n      delivered " + count +
                                " times in blocks : " + inBlocks
                );
                duplicatedCount += count;
            }
        }
        this.getOwner().getLogger().info(
                "total duplicated transactions count : " + duplicatedCount
        );
        // ***
        if(numBlocks < 4) {
            throw new RuntimeException("there must be at least 4 delivered blocks to collect relevant metrics");
        }
        numberOfTransactionsPerBlock.sort(Integer::compareTo);
        Integer minBlockSize = numberOfTransactionsPerBlock.get(0);
        Integer firstQuartileBlockSize = numberOfTransactionsPerBlock.get((numBlocks/4)-1);
        Integer medianBlockSize = numberOfTransactionsPerBlock.get((numBlocks/2)-1);
        Integer thirdQuartileBlockSize = numberOfTransactionsPerBlock.get(((3*numBlocks)/4)-1);
        Integer maxBlockSize = numberOfTransactionsPerBlock.get(numBlocks - 1);
        this.getOwner().getLogger().info(
                "among " + numBlocks + " blocks/waves : " +
                " every block/wave has at least " + minBlockSize +
                        " transactions ; 25% of blocks/waves have at least " + firstQuartileBlockSize +
                        " transactions ; 50% of blocks/waves have at least " + medianBlockSize +
                        " transactions ; 75% of blocks/waves have at least " + thirdQuartileBlockSize +
                        " transactions ; the maximum number of transactions in a block is " + maxBlockSize
        );
        // ***
        // Print results as a line in a CSV file
        if (this.outputCsvPath.isPresent()) {
            String outputCsvFilePath = this.outputCsvPath.get();
            String line1 = "numValidators;minNumBlocks;q1NumBlocks;medNumBlocks;q3NumBlocks;maxNumBlocks;duplicated;minBlockSize;q1BlockSize;medBlockSize;q3BlockSize;maxBlockSize";
            Integer numValidators = this.getOwner().getAgentsWithRole(this.getEnvironment(), this.validatorRole).size();
            String line2 = numValidators + ";" + blocksDistributionMetricsAcrossValidators.get(0) +
                    ";" + blocksDistributionMetricsAcrossValidators.get(1) +
                    ";" + blocksDistributionMetricsAcrossValidators.get(2) +
                    ";" + blocksDistributionMetricsAcrossValidators.get(3) +
                    ";" + blocksDistributionMetricsAcrossValidators.get(4) +
                    ";" + duplicatedCount +
                    ";" + minBlockSize +
                    ";" + firstQuartileBlockSize +
                    ";" + medianBlockSize +
                    ";" + thirdQuartileBlockSize +
                    ";" + maxBlockSize;
            try {
                FileWriter fw = new FileWriter(outputCsvFilePath, false);
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write( line1 + "\n" + line2 );
                bw.newLine();
                bw.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public <T extends Action<StochasticP2PEnvironment>> T copy() {
        return (T) new AcCollectDeliveredTransactionsMetrics(
                getEnvironment(),
                getOwner(),
                this.validatorRole,
                this.outputCsvPath);
    }

}
