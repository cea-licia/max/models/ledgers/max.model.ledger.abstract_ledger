/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.action.config;

import max.core.action.Action;
import max.model.ledger.abstract_ledger.approval.AbstractTransactionApprovalPolicy;
import max.model.ledger.abstract_ledger.env.LedgerValidatorContext;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import max.model.network.stochastic_adversarial_p2p.role.RStochasticNetworkPeer;


/**
 * Action executed when (re)setting the transaction approval policy of a specific validator of the ledger.
 *
 * @author Erwan Mahe
 */
public class AcSetLocalApprovalPolicy<T_tx, T_st extends AbstractLocalLedgerState<T_tx>,  A extends StochasticPeerAgent> extends Action<A> {

    private final AbstractTransactionApprovalPolicy<T_tx,T_st> approvalPolicy;

    public AcSetLocalApprovalPolicy(String environmentName, A owner, AbstractTransactionApprovalPolicy<T_tx,T_st> approvalPolicy) {
        super(environmentName, RStochasticNetworkPeer.class, owner);
        this.approvalPolicy = approvalPolicy;
    }

    @Override
    public void execute() {
        LedgerValidatorContext<T_tx,T_st> context = (LedgerValidatorContext<T_tx,T_st>) this.getOwner().getContext(this.getEnvironment());
        getOwner().getLogger().info("(Re)initializing application-side transaction approval policy");
        if (context.approvalPolicy == null) {
            getOwner().getLogger().info(
                    "initializing ledger validator transactions approval policy"
            );
        } else {
            getOwner().getLogger().warning(
                    "RESET ledger validator transactions approval policy"
            );
        }
        context.approvalPolicy = this.approvalPolicy;
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new AcSetLocalApprovalPolicy(getEnvironment(), getOwner(), this.approvalPolicy.copy_as_new());
    }
}
