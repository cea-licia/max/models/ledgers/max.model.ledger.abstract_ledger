/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.action.config;

import max.core.action.Action;
import max.model.ledger.abstract_ledger.env.LedgerValidatorContext;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import max.model.network.stochastic_adversarial_p2p.role.RStochasticNetworkPeer;



/**
 * Action executed when (re)setting the local ledger state of a specific validator of the ledger.
 *
 * @author Erwan Mahe
 */
public class AcSetLocalLedgerState<T_tx, T_st extends AbstractLocalLedgerState<T_tx>, A extends StochasticPeerAgent> extends Action<A> {

    private final T_st newLedgerState;

    public AcSetLocalLedgerState(String environmentName, A owner, T_st newLedgerState) {
        super(environmentName, RStochasticNetworkPeer.class, owner);
        this.newLedgerState = newLedgerState;
    }

    @Override
    public void execute() {
        LedgerValidatorContext<T_tx,T_st> context = (LedgerValidatorContext<T_tx,T_st>) this.getOwner().getContext(this.getEnvironment());
        getOwner().getLogger().info("(Re)initializing application-side ledger validator state");
        if (context.ledgerValidatorState == null) {
            getOwner().getLogger().info(
                    "initializing ledger validator state of type :" + this.newLedgerState.getDescription()
            );
        } else {
            getOwner().getLogger().warning(
                    "RESET ledger validator state of type :" + this.newLedgerState.getDescription()
            );
        }
        context.ledgerValidatorState = this.newLedgerState;
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new AcSetLocalLedgerState(getEnvironment(), getOwner(), this.newLedgerState.copy_as_new());
    }
}
