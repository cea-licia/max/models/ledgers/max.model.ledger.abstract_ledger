/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.action.emission;


import madkit.kernel.AgentAddress;
import max.core.action.Action;
import max.core.role.Role;
import max.core.scheduling.ActionActivator;
import max.core.scheduling.ActivationScheduleFactory;
import max.model.ledger.abstract_ledger.action.transact.AcReceiveTransactionToPutInMempool;
import max.model.ledger.abstract_ledger.role.RAbstractLedgerChannel;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import max.model.network.stochastic_adversarial_p2p.context.delay.DelaySpecification;
import max.model.network.stochastic_adversarial_p2p.env.StochasticP2PEnvironment;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;


/**
 * Send one transaction to nodes of the ledger for later inclusion in block/vertices proposals.
 *
 * @author Erwan Mahe
 */
public class AcOneTransactionBroadcast<T_tx, T_st extends AbstractLocalLedgerState<T_tx>> extends Action<StochasticP2PEnvironment> {

    private final Class<? extends Role> targetRole;

    private final Optional<DelaySpecification> transmissionDelaySpecification;

    private final T_tx transaction;

    private final LedgerTransactionBroadcastFromClientToNodesPolicy policy;

    private final Optional<TransactionSendOrderTracker> sendOrderTracker;

    public AcOneTransactionBroadcast(String environment,
                                     StochasticP2PEnvironment owner,
                                     Class<? extends Role> targetRole,
                                     Optional<DelaySpecification> transmissionDelaySpecification,
                                     T_tx transaction,
                                     LedgerTransactionBroadcastFromClientToNodesPolicy policy,
                                     Optional<TransactionSendOrderTracker> sendOrderTracker) {
        super(environment, RAbstractLedgerChannel.class, owner);
        this.targetRole = targetRole;
        this.transmissionDelaySpecification = transmissionDelaySpecification;
        this.transaction = transaction;
        this.policy = policy;
        this.sendOrderTracker = sendOrderTracker;
    }


    @Override
    public void execute() {
        this.sendOrderTracker.ifPresent(
                tracker -> tracker.track(this.transaction)
        );
        //
        getOwner().getLogger().info(
                "transaction " + this.transaction.toString() +
                        " to be broadcast to all the validators of role " + this.targetRole.getName() +
                        " in environment " + this.getOwner().getName());
        //
        ArrayList<AgentAddress> availableValidators = new ArrayList<>(
                this.getOwner().getAgentsWithRole(this.getEnvironment(), this.targetRole)
        );
        //
        if (this.policy.restrictToSpecificNamedNodes.isPresent()) {
            HashSet<String> subsetOfNodes = this.policy.restrictToSpecificNamedNodes.get();
            availableValidators = availableValidators.stream().filter(
                    x -> subsetOfNodes.contains(x.getAgent().getName())
            ).collect(Collectors.toCollection(ArrayList::new));
        }
        //
        if (this.policy.restrictToRandomlySelectedSubsetOfNodes.isPresent()) {
            int remaining = this.policy.restrictToRandomlySelectedSubsetOfNodes.get();
            ArrayList<AgentAddress> retainedValidators = new ArrayList<>();
            Random rand = new Random();
            while (remaining > 0 && availableValidators.size() > 0) {
                int idx = rand.nextInt(availableValidators.size());
                AgentAddress agentAddress = availableValidators.get(idx);
                availableValidators.remove(agentAddress);
                retainedValidators.add(agentAddress);
                remaining -= 1;
            }
            availableValidators = retainedValidators;
        }
        //
        for (AgentAddress agentAddress : availableValidators) {
            StochasticPeerAgent agent = (StochasticPeerAgent) agentAddress.getAgent();
            Action<?> action = new AcReceiveTransactionToPutInMempool(
                    this.getEnvironment(),
                    agent,
                    this.transaction);
            int delay = 0;
            if (this.transmissionDelaySpecification.isPresent()) {
                delay = delay + this.transmissionDelaySpecification.get().get_concrete_delay();
            }
            getOwner().getLogger().info(
                    "validator node " + agent.getName() + " will receive it after " + delay);
            final var nextTime = getOwner().getSimulationTime()
                    .getCurrentTick().add(BigDecimal.valueOf(delay));
            getOwner().schedule(
                    new ActionActivator<>(
                            ActivationScheduleFactory.createOneTime(nextTime),
                            action
                    ));
        }
    }

    @Override
    public <T extends Action<StochasticP2PEnvironment>> T copy() {
        return (T) new AcOneTransactionBroadcast(
                getEnvironment(),
                getOwner(),
                this.targetRole,
                this.transmissionDelaySpecification,
                this.transaction,
                this.policy,
                this.sendOrderTracker);
    }

}
