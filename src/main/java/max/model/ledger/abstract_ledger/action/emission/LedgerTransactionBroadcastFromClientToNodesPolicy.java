/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/


package max.model.ledger.abstract_ledger.action.emission;

import java.util.HashSet;
import java.util.Optional;


/**
 * Specifies how to send a new transaction directly to nodes of the ledger so that they include
 * it in their local mempool for later inclusion
 * into a block/vertex proposal.
 *
 * @author Erwan Mahe
 */
public class LedgerTransactionBroadcastFromClientToNodesPolicy {

    public final Optional<HashSet<String>> restrictToSpecificNamedNodes;

    public final Optional<Integer> restrictToRandomlySelectedSubsetOfNodes;


    public LedgerTransactionBroadcastFromClientToNodesPolicy(Optional<HashSet<String>> restrictToSpecificNamedNodes, Optional<Integer> restrictToRandomlySelectedSubsetOfNodes) {
        this.restrictToSpecificNamedNodes = restrictToSpecificNamedNodes;
        this.restrictToRandomlySelectedSubsetOfNodes = restrictToRandomlySelectedSubsetOfNodes;
    }
}
