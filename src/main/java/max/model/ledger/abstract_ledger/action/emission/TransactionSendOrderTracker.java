/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.action.emission;


/**
 * A specific implementation of a send order tracker.
 * A basic trivial implementation would be a list.
 * But it may become impractical when there are many transactions.
 * Moreover some transactions commute and their relative emission order is not interesting.
 * So, depending on the nature of the transactions, this tracker could implement things differently.
 *
 * @author Erwan Mahe
 */
public interface TransactionSendOrderTracker {

    void track(Object transaction);

}
