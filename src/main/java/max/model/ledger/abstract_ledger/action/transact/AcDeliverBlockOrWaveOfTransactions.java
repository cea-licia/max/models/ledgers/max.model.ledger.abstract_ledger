/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.action.transact;

import java.util.List;

import max.core.action.Action;
import max.model.ledger.abstract_ledger.env.LedgerValidatorContext;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import max.model.network.stochastic_adversarial_p2p.role.RStochasticNetworkPeer;


/**
 * Action executed when a local ledger validator receives a transaction
 * (directly from a client or via gossip from another validator)
 * Then, if the transaction is valid (depending on the validator's approval policy),
 * it puts it in its local mempool
 *
 * @author Erwan Mahe
 */
public class AcDeliverBlockOrWaveOfTransactions<T_tx, T_st extends AbstractLocalLedgerState<T_tx>, A extends StochasticPeerAgent> extends Action<A> {

    private final List<T_tx> block;

    public AcDeliverBlockOrWaveOfTransactions(String environmentName, A owner, List<T_tx> block) {
        super(environmentName, RStochasticNetworkPeer.class, owner);
        this.block = block;
    }

    @Override
    public void execute() {
        LedgerValidatorContext<T_tx,T_st> context = (LedgerValidatorContext<T_tx,T_st>) this.getOwner().getContext(this.getEnvironment());
        getOwner().getLogger().info("DELIVERING a block/wave");
        // purge the local mempool of the transactions that are in the block
        int numPurgedFromBlockDelivery = context.clientTransactionsMempool.purgeMempoolOfDeliveredBlock(this.block);
        getOwner().getLogger().info("purged local mempool from " + numPurgedFromBlockDelivery + " transactions that are in the delivered block");
        // modify the local ledger state via block delivery
        getOwner().getLogger().info("updating local ledger state via delivery operations of transactions in the block");
        context.ledgerValidatorState.onLedgerDeliverBlock(this.block, this.getOwner().getLogger());
        // because the local ledger state has changed, some transactions in the mempool that were valid may not be valid anymore
        int numPurgedFromUpdatedPolicy = context.clientTransactionsMempool.purgeMempoolWithUpdatedAprovalPolicy(
                context.approvalPolicy,
                context.ledgerValidatorState
        );
        getOwner().getLogger().info("purged local mempool of " + numPurgedFromUpdatedPolicy + " no-longer-valid transactions given local ledger state having changed");
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new AcDeliverBlockOrWaveOfTransactions(getEnvironment(), getOwner(), this.block);
    }
}
