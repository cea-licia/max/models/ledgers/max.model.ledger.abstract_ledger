/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.action.transact;

import max.core.action.Action;
import max.model.ledger.abstract_ledger.env.LedgerValidatorContext;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import max.model.network.stochastic_adversarial_p2p.role.RStochasticNetworkPeer;


/**
 * Action executed when a local ledger validator receives a transaction
 * (directly from a client or via gossip from another validator)
 * Then, if the transaction is valid (depending on the validator's approval policy),
 * it puts it in its local mempool
 *
 * @author Erwan Mahe
 */
public class AcReceiveTransactionToPutInMempool<T_tx, T_st extends AbstractLocalLedgerState<T_tx>, A extends StochasticPeerAgent> extends Action<A> {

    private final T_tx tx;

    public AcReceiveTransactionToPutInMempool(String environmentName, A owner, T_tx tx) {
        super(environmentName, RStochasticNetworkPeer.class, owner);
        this.tx = tx;
    }

    @Override
    public void execute() {
        LedgerValidatorContext<T_tx,T_st> context = (LedgerValidatorContext<T_tx,T_st>) this.getOwner().getContext(this.getEnvironment());
        if (context.approvalPolicy.approve(tx,context.ledgerValidatorState)) {
            boolean alreadyInMempool = context.clientTransactionsMempool.addTransactionToMempool(tx);
            if (alreadyInMempool) {
                this.getOwner().getLogger().fine("IGNORING duplicated transaction in mempool : " + tx.toString());
            } else {
                this.getOwner().getLogger().fine("ADDED the following transaction to mempool : " + tx.toString());
                context.ledgerValidatorState.onLedgerReceiveTransaction(this.tx, this.getOwner().getLogger());
            }
        } else {
            this.getOwner().getLogger().fine("IGNORING invalid transaction : " + tx.toString());
        }
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new AcReceiveTransactionToPutInMempool(getEnvironment(), getOwner(), this.tx);
    }
}
