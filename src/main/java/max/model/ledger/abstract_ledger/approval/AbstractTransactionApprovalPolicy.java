/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.approval;


import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;

/**
 * An abstract and generic policy to approve transactions.
 *
 * @author Erwan Mahe
 */
public interface AbstractTransactionApprovalPolicy<T_tx, T_st extends AbstractLocalLedgerState<T_tx>> {
    boolean approve(T_tx transactionToApprove, T_st localLedgerState);

    AbstractTransactionApprovalPolicy<T_tx, T_st> copy_as_new();
}
