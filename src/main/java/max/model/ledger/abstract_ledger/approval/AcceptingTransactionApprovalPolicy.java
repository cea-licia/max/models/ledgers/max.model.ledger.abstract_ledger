/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.approval;


import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;

/**
 * Always approve transactions.
 *
 * @author Erwan Mahe
 */
public class AcceptingTransactionApprovalPolicy<T_tx, T_st extends AbstractLocalLedgerState<T_tx>> implements AbstractTransactionApprovalPolicy<T_tx,T_st> {

    @Override
    public boolean approve(T_tx transactionToApprove, T_st localLedgerState) {
        return true;
    }

    @Override
    public AbstractTransactionApprovalPolicy<T_tx, T_st> copy_as_new() {
        return new AcceptingTransactionApprovalPolicy();
    }

}
