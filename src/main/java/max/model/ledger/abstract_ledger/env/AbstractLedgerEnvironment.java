/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.env;

import max.core.Context;
import max.core.agent.SimulatedAgent;
import max.model.ledger.abstract_ledger.role.RLedgerValidator;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import max.model.network.stochastic_adversarial_p2p.env.StochasticP2PEnvironment;

import java.util.logging.Level;

/**
 * An environment for the abstract ledger.
 *
 * @author Erwan Mahe
 */
public class AbstractLedgerEnvironment<T_tx, T_st extends AbstractLocalLedgerState<T_tx>> extends StochasticP2PEnvironment {

    public AbstractLedgerEnvironment() {
        super();
        getLogger().setLevel(Level.INFO);
        addAllowedRole(RLedgerValidator.class);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    protected Context createContext(SimulatedAgent agent) {
        return new LedgerValidatorContext<T_tx,T_st>((StochasticPeerAgent) agent, this);
    }

}
