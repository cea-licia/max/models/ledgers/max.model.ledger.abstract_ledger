/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.env;


import max.model.ledger.abstract_ledger.approval.AbstractTransactionApprovalPolicy;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import max.model.network.stochastic_adversarial_p2p.context.StochasticP2PContext;
import max.model.network.stochastic_adversarial_p2p.env.StochasticP2PEnvironment;



/**
 * Context for validators of a ledger that have an (application-side) state that is notified and/or modified by
 * receiving transactions (e.g., check_tx)
 * delivering transactions (e.g., deliver_tx)
 *
 * @author Erwan Mahe
 */
public class LedgerValidatorContext<T_tx, T_st extends AbstractLocalLedgerState<T_tx>> extends StochasticP2PContext {

    /**
     * Specifies (Application Side) what happens when transactions are
     * received and delivered
     * **/
    public T_st ledgerValidatorState;

    /**
     * Specifies whether or not transactions are valid.
     * **/
    public AbstractTransactionApprovalPolicy<T_tx,T_st> approvalPolicy;

    /**
     * Local mempool from the point of view of the validator.
     * It stores application-layer transactions received either directly from clients or from other validators via gossip.
     * **/
    public final ValidatorLocalClientTransactionsMempool<T_tx,T_st> clientTransactionsMempool;


    public LedgerValidatorContext(StochasticPeerAgent owner, StochasticP2PEnvironment environment) {
        super(owner, environment);
        this.clientTransactionsMempool = new ValidatorLocalClientTransactionsMempool<>();
    }



}
