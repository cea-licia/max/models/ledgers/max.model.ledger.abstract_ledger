/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/



package max.model.ledger.abstract_ledger.env;

import madkit.kernel.AgentLogger;
import max.model.ledger.abstract_ledger.approval.AbstractTransactionApprovalPolicy;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Local Mempool from the point of view of a Ledger Validator.
 *
 * It stores Application-Layer transactions that were received either directly from clients or via gossip from other validators.
 * These transactions are then used as the input from which to take transactions and add them to the ledger.
 *
 * @author Erwan Mahe
 */
public class ValidatorLocalClientTransactionsMempool<T_tx, T_st extends AbstractLocalLedgerState<T_tx>> {

    private List<T_tx> transactions;

    public ValidatorLocalClientTransactionsMempool() {
        this.transactions = new ArrayList<>();
    }

    /**
     * Adds a newly received transaction to the mempool, ignoring duplicated transactions.
     * If transaction already in mempool, returns true;
     * **/
    public boolean addTransactionToMempool(T_tx tx) {
        if (this.transactions.contains(tx)) {
            return true;
        } else {
            this.transactions.add(tx);
            return false;
        }
    }

    /**
     * Whenever a new block/wave of transactions is delivered (i.e. finalized in the ledger),
     * we remove all of them from the mempool.
     * **/
    public int purgeMempoolOfDeliveredBlock(List<T_tx> deliveredBlock) {
        int oldNum = this.transactions.size();
        this.transactions = this.transactions.stream().filter(tx -> !(deliveredBlock.contains(tx)))
                .collect(Collectors.toCollection(ArrayList::new));
        int newNum = this.transactions.size();
        return oldNum - newNum;
    }

    /**
     * If the approval policy of a validator node changes, we should also purge its local mempool
     * from transactions that are no longer valid.
     * **/
    public int purgeMempoolWithUpdatedAprovalPolicy(AbstractTransactionApprovalPolicy<T_tx,T_st> approvalPolicy, T_st localLedgerState) {
        int oldNum = this.transactions.size();
        this.transactions = this.transactions.stream().filter(t -> approvalPolicy.approve(t,localLedgerState))
                .collect(Collectors.toCollection(ArrayList::new));
        int newNum = this.transactions.size();
        return oldNum - newNum;
    }

    /**
     * If a validator wants to propose a new block/vertex of transactions to be considered for addition to the ledger,
     * it will extract some transactions from its own local mempool.
     * This is what this method does.
     * The "maximumNumber" gives the option to limit the number of extracted transactions.
     * The "transactionsPrioritizer" gives the option to prioritize certain transactions over others
     *   this can be used to represent, e.g. "gas rewards", validators being incentivized to include transactions with higher rewards in Proof of Stake blockchains
     * **/
    public List<T_tx> extractTransactionsForProposal(
            Optional<Comparator<T_tx>> transactionsPrioritizer,
            Optional<Integer> maximumNumber,
            AbstractTransactionApprovalPolicy<T_tx,T_st> approvalPolicy,
            T_st localLedgerState) {
        List<T_tx> txsToConsider;
        if (transactionsPrioritizer.isEmpty()) {
            txsToConsider = this.transactions.stream().filter(t -> approvalPolicy.approve(t,localLedgerState))
                    .collect(Collectors.toCollection(ArrayList::new));
        } else {
            Comparator<T_tx> comparator = transactionsPrioritizer.get();
            txsToConsider = this.transactions.stream().filter(t -> approvalPolicy.approve(t,localLedgerState)).sorted(comparator)
                    .collect(Collectors.toCollection(ArrayList::new));
        }
        if (maximumNumber.isEmpty()) {
            return txsToConsider;
        } else {
            return txsToConsider.stream().limit(maximumNumber.get()).collect(Collectors.toList());
        }
    }

}
