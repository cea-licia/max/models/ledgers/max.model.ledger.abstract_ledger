/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.role;

import max.model.network.stochastic_adversarial_p2p.role.RStochasticNetworkPeer;

/**
 * Role taken by the validators in the ledger.
 *
 * @author Erwan Mahe
 */
public interface RLedgerValidator extends RStochasticNetworkPeer {}
