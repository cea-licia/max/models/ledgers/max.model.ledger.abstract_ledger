/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.state;


import madkit.kernel.AgentLogger;

import java.util.List;

/**
 * Interface for handling the reception and delivery of transactions
 * (application side) by the validators of a distributed ledger
 *
 * @author Erwan Mahe
 */
public interface AbstractLocalLedgerState<T_tx> {

    String getDescription();

    /**
     * called when the validator receives an application-side transaction either from a client or through gossip
     * **/
    void onLedgerReceiveTransaction(T_tx transaction, AgentLogger logger);

    /**
     * called when a block/wave (for Blockchains/DAG-based ledgers) is delivered/finalized
     * **/
    void onLedgerDeliverBlock(List<T_tx> block, AgentLogger logger);

    /**
     * returns the ordered list of blocks (lists of transactions) from the local point of view of the validator
     * this returns the local copy of the blockchain (for blockchain ledgers)
     * **/
    List<List<T_tx>> getOrderedBlocks();

    AbstractLocalLedgerState<T_tx> copy_as_new();
}
