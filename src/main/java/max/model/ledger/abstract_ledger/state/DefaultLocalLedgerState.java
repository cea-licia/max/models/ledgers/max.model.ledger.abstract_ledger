/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.state;

import madkit.kernel.AgentLogger;
import java.util.ArrayList;
import java.util.List;


/**
 * Default Local State of a Validator for a Distributed Ledger.
 *
 * @author Erwan Mahe
 */
public class DefaultLocalLedgerState<T_tx> implements AbstractLocalLedgerState<T_tx> {

    private List<List<T_tx>> orderedBlocks;

    /**
     * Default constructor.
     *
     * We initialize all transaction storage as empty.
     */
    public DefaultLocalLedgerState() {
        this.orderedBlocks = new ArrayList<>();
    }

    @Override
    public String getDescription() {
        return "Default";
    }

    @Override
    public void onLedgerReceiveTransaction(T_tx transaction, AgentLogger logger) {
        // do nothing
    }

    @Override
    public void onLedgerDeliverBlock(List<T_tx> block, AgentLogger logger) {
        this.orderedBlocks.add(block);
    }

    @Override
    public List<List<T_tx>> getOrderedBlocks() {
        return this.orderedBlocks;
    }

    @Override
    public AbstractLocalLedgerState<T_tx> copy_as_new() {
        return new DefaultLocalLedgerState<>();
    }

}
