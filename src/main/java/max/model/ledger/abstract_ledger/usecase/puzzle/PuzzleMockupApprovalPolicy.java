/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.usecase.puzzle;


import max.model.ledger.abstract_ledger.approval.AbstractTransactionApprovalPolicy;

import java.util.HashSet;

/**
 * Always approve transactions.
 *
 * @author Erwan Mahe
 */
public class PuzzleMockupApprovalPolicy implements AbstractTransactionApprovalPolicy<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState> {
    private HashSet<String> ostracizedClients;

    public PuzzleMockupApprovalPolicy(HashSet<String> ostracizedClients) {
        this.ostracizedClients = ostracizedClients;
    }

    @Override
    public boolean approve(PuzzleMockupTransaction transactionToApprove, PuzzleMockupLocalLedgerState localLedgerState) {
        if (transactionToApprove.clientName.isPresent()) {
            String clientName = transactionToApprove.clientName.get();
            if (this.ostracizedClients.contains(clientName)) {
                // we always refuse puzzle solutions from ostracized clients
                return false;
            } else {
                if (localLedgerState.hasPuzzleSolutionAlreadyBeenDelivered(transactionToApprove)) {
                    // we refuse already delivered puzzle solutions to avoid duplicated transactions
                    return false;
                } else {
                    return true;
                }
            }
        } else {
            // we always approve heartbeat transactions
            return true;
        }
    }

    @Override
    public AbstractTransactionApprovalPolicy<PuzzleMockupTransaction, PuzzleMockupLocalLedgerState> copy_as_new() {
        return new PuzzleMockupApprovalPolicy(new HashSet<>(this.ostracizedClients));
    }

}
