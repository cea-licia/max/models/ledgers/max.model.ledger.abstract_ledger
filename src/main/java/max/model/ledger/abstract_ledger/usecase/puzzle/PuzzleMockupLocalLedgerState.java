/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.usecase.puzzle;

import madkit.kernel.AgentLogger;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Local State of a Validator for a Distributed Ledger that deals with transactions of our Mockup Puzzle usecase.
 * Via this state definition, we can evaluate the respect of various order-fairness properties in practice:
 * - receive-order fairness
 *   i.e. if the local order of reception of puzzle solutions is respected in the final delivery order
 * - block/wave-order fairness
 *   i.e. if the local order of reception of puzzle solutions is respected by the content of each block in blockchains or waves in DAG-based ledgers
 * - differential-order fairness
 *   i.e. a more specific variant on receive-order fairness
 *
 * We can also trivially verify a weak form of "client-fairness" via the likelihood of each client to win
 * following multiple consecutive instances of puzzles
 *
 * @author Erwan Mahe
 */
public class PuzzleMockupLocalLedgerState implements AbstractLocalLedgerState<PuzzleMockupTransaction> {

    private final List<List<PuzzleMockupTransaction>> orderedBlocks;

    // the order with which the validator received solutions submitted by clients for each puzzle
    // for each puzzle, we are guaranteed to have all clients ordered iff the
    // system is parameterized so that all clients always broadcast their transactions
    // to all the nodes systematically
    public HashMap<Integer, List<String>> localReceiveOrderPerPuzzle;

    // the order with which the solutions (submitted by clients) were delivered for each puzzle
    // additionally, we precise the block/wave number as part of which the transaction is delivered
    public HashMap<Integer, List<Pair<String,Integer>>> deliveryOrderPerPuzzle;

    // for each client that submits solutions, the number of times it won
    // i.e. that its submitted solution was ordered first for a given puzzle
    public HashMap<String,Integer> numberOfVictoriesPerClient;

    // keeps track of the number of times, for each puzzle and client
    // the given node received the corresponding message
    // in case one uses gossip algorithms, this can help evaluate
    // the superfluous gossip overhead, as the same node will receive several times the same message
    public HashMap<Pair<Integer,String>,Integer> duplicatedStutteredReceptions;

    /**
     * Default constructor.
     *
     * We initialize all transaction storage as empty.
     */
    public PuzzleMockupLocalLedgerState() {
        this.orderedBlocks = new ArrayList<>();
        this.localReceiveOrderPerPuzzle = new HashMap<>();
        this.deliveryOrderPerPuzzle = new HashMap<>();
        this.numberOfVictoriesPerClient = new HashMap<>();
        this.duplicatedStutteredReceptions = new HashMap<>();
    }


    @Override
    public String getDescription() {
        return "PuzzleMockup";
    }

    public boolean hasPuzzleSolutionAlreadyBeenDelivered(PuzzleMockupTransaction transaction) {
        if (transaction.clientName.isPresent()) {
            String clientName = transaction.clientName.get();
            if (this.deliveryOrderPerPuzzle.containsKey(transaction.puzzleIdentifier)) {
                List<Pair<String,Integer>> orderOnPuzzle = this.deliveryOrderPerPuzzle.get(transaction.puzzleIdentifier);
                return orderOnPuzzle.stream().map(Pair::getLeft).anyMatch(c -> c.equals(clientName));
            } else {
                return false;
            }
        } else {
            // we always consider heartbeat transactions to be new
            return false;
        }
    }

    @Override
    public void onLedgerReceiveTransaction(
            PuzzleMockupTransaction transaction,
            AgentLogger logger
    ) {
        if (transaction.clientName.isPresent()) {
            String clientName = transaction.clientName.get();
            if (this.localReceiveOrderPerPuzzle.containsKey(transaction.puzzleIdentifier)) {
                List<String> orderOnPuzzle = this.localReceiveOrderPerPuzzle.get(transaction.puzzleIdentifier);
                Pair<Integer,String> identifierForStutterCount = Pair.of(transaction.puzzleIdentifier,clientName);
                if (!orderOnPuzzle.contains(clientName)) {
                    orderOnPuzzle.add(clientName);
                    this.localReceiveOrderPerPuzzle.put(transaction.puzzleIdentifier, orderOnPuzzle);
                    this.duplicatedStutteredReceptions.put(identifierForStutterCount,1);
                    logger.info("transaction " + transaction + " received for the first time");
                } else {
                    this.duplicatedStutteredReceptions.put(
                            identifierForStutterCount,
                            this.duplicatedStutteredReceptions.get(identifierForStutterCount)+1
                    );
                    logger.info("transaction " + transaction + " received once again");
                }
            } else {
                List<String> orderOnPuzzle = new ArrayList<>();
                orderOnPuzzle.add(clientName);
                this.localReceiveOrderPerPuzzle.put(transaction.puzzleIdentifier, orderOnPuzzle);
            }
        }
    }

    @Override
    public void onLedgerDeliverBlock(List<PuzzleMockupTransaction> block,
                                     AgentLogger logger) {
        int blockNum = this.orderedBlocks.size() + 1;
        logger.info("Delivering block/wave " + blockNum + " with " + block.size() + " transactions");
        for (PuzzleMockupTransaction tr : block) {
            this.onLedgerDeliver(blockNum, tr, logger);
        }
        this.orderedBlocks.add(block);
    }

    @Override
    public List<List<PuzzleMockupTransaction>> getOrderedBlocks() {
        return new ArrayList<>(this.orderedBlocks);
    }

    @Override
    public AbstractLocalLedgerState<PuzzleMockupTransaction> copy_as_new() {
        return new PuzzleMockupLocalLedgerState();
    }

    private void onLedgerDeliver(
            int blockOrWaveNumber,
            PuzzleMockupTransaction transaction,
            AgentLogger logger) {
        // ***
        if (transaction.clientName.isPresent()) {
            String clientName = transaction.clientName.get();
            if (this.deliveryOrderPerPuzzle.containsKey(transaction.puzzleIdentifier)) {
                List<Pair<String,Integer>> orderOnPuzzle = this.deliveryOrderPerPuzzle.get(transaction.puzzleIdentifier);
                boolean alreadyIn = false;
                for (Pair<String,Integer> got : orderOnPuzzle) {
                    if (got.getLeft().equals(clientName)) {
                        alreadyIn = true;
                        logger.info("Duplicated Transaction delivered :\n - from " +
                                clientName + " for puzzle " + transaction.puzzleIdentifier +
                                "\n - at first in block/wave " + got.getRight() +
                                " and then a second time in block/wave " + blockOrWaveNumber
                        );
                        break;
                    }
                }
                if (!alreadyIn) {
                    orderOnPuzzle.add(Pair.of(clientName,blockOrWaveNumber));
                    this.deliveryOrderPerPuzzle.put(transaction.puzzleIdentifier, orderOnPuzzle);
                }
            } else {
                // ***
                logger.info("Client " + clientName + " WON puzzle " + transaction.puzzleIdentifier);
                if (numberOfVictoriesPerClient.containsKey(clientName)) {
                    int victories = numberOfVictoriesPerClient.get(clientName);
                    victories = victories + 1;
                    numberOfVictoriesPerClient.put(clientName,victories);
                } else {
                    numberOfVictoriesPerClient.put(clientName,1);
                }
                // ***
                List<Pair<String,Integer>> orderOnPuzzle = new ArrayList<>();
                orderOnPuzzle.add(Pair.of(clientName,blockOrWaveNumber));
                this.deliveryOrderPerPuzzle.put(transaction.puzzleIdentifier, orderOnPuzzle);
            }
        }
    }

}
