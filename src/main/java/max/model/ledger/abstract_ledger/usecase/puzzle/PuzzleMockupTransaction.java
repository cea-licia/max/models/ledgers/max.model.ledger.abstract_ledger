/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.usecase.puzzle;


import org.apache.commons.lang3.tuple.Pair;

import java.util.Optional;

/**
 * Puzzle Mockup transactions.
 * It is supposed to replace the generic type T_tx
 *
 * If the clientName is Optional.empty() then this is a "heartbeat" transaction, just to keep the system live.
 * These heartbeat transactions are not stored in the ledger state nor delivered.
 *
 * @author Erwan Mahe
 */
public class PuzzleMockupTransaction {

    public Optional<String> clientName;

    public int puzzleIdentifier;

    public PuzzleMockupTransaction(Optional<String> clientName, int puzzleIdentifier) {
        this.clientName = clientName;
        this.puzzleIdentifier = puzzleIdentifier;
    }

    public String textualRepresentation() {
        return this.puzzleIdentifier + ":" + this.clientName;
    }

    @Override
    public String toString() {
        return "(" + this.clientName + "," + this.puzzleIdentifier + ")";
    }

    @Override
    public int hashCode() {
        return Pair.of(this.clientName,this.puzzleIdentifier).hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        // If the object is compared with itself then return true
        if (o == this) {
            return true;
        }
        // If the object is not a PuzzleMockupTransaction then return false
        if (!(o instanceof PuzzleMockupTransaction)) {
            return false;
        }
        // typecast o to PuzzleMockupTransaction
        PuzzleMockupTransaction other = (PuzzleMockupTransaction) o;
        // compare puzzle identifiers
        if (this.puzzleIdentifier != other.puzzleIdentifier) {
            return false;
        }
        // compare client names
        if (this.clientName.isPresent()) {
            String thisClientName = this.clientName.get();
            if (other.clientName.isPresent()) {
                String otherClientName = other.clientName.get();
                if (!thisClientName.equals(otherClientName)) {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            if (other.clientName.isPresent()) {
                return false;
            }
        }
        return true;
    }
}
