/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.usecase.puzzle;

import java.util.HashMap;
import java.util.Optional;


/**
 * A Singleton generator for mockup transactions.
 * These transactions can be used to keep the system alive in simulations
 * by artificially creating new transactions.
 *
 * @author Erwan Mahe
 */
public final class PuzzleMockupTransactionGenerator {


    // The field must be declared volatile
    private static volatile PuzzleMockupTransactionGenerator instance;

    private final HashMap<String,Integer> clientToCounter;

    private Integer hearBeatCounter;

    private PuzzleMockupTransactionGenerator() {
        this.clientToCounter = new HashMap<>();
        this.hearBeatCounter = 0;
    }

    public static PuzzleMockupTransactionGenerator getInstance() {
        // Double-checked locking (DCL) prevents race condition between multiple
        // threads that may attempt to get singleton instance at the same time,
        // creating separate instances as a result.
        //
        // The `result` variable is important here.
        //
        // https://refactoring.guru/fr/design-patterns/singleton
        // https://refactoring.guru/java-dcl-issue
        PuzzleMockupTransactionGenerator result = instance;
        if (result != null) {
            return result;
        }
        synchronized(PuzzleMockupTransactionGenerator.class) {
            if (instance == null) {
                instance = new PuzzleMockupTransactionGenerator();
            }
            return instance;
        }
    }

    public static void resetGenerator() {
        instance = null;
    }

    public PuzzleMockupTransaction createNewTransactionForClient(String clientName) {
        int val = this.clientToCounter.getOrDefault(clientName, 1);
        this.clientToCounter.put(clientName, val + 1);
        return new PuzzleMockupTransaction(Optional.of(clientName),val);
    }

    public PuzzleMockupTransaction createNewHeartbeatTransaction() {
        int val = this.hearBeatCounter;
        this.hearBeatCounter += 1;
        return new PuzzleMockupTransaction(Optional.empty(),val);
    }

}