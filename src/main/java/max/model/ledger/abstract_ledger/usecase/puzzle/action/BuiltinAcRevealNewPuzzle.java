/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.usecase.puzzle.action;


import max.core.action.Action;
import max.core.role.Role;
import max.core.scheduling.ActionActivator;
import max.core.scheduling.ActivationScheduleFactory;
import max.model.ledger.abstract_ledger.action.emission.AcOneTransactionBroadcast;
import max.model.ledger.abstract_ledger.action.emission.LedgerTransactionBroadcastFromClientToNodesPolicy;
import max.model.ledger.abstract_ledger.action.emission.TransactionSendOrderTracker;
import max.model.ledger.abstract_ledger.role.RAbstractLedgerChannel;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupTransaction;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupTransactionGenerator;
import max.model.network.stochastic_adversarial_p2p.context.delay.DelaySpecification;
import max.model.network.stochastic_adversarial_p2p.env.StochasticP2PEnvironment;
import org.apache.commons.lang3.tuple.Pair;

import java.math.BigDecimal;
import java.util.*;


/**
 * Action executed by the environment to simulate
 * clients broadcasting new puzzle solutions
 * to validators of the ledger.
 *
 * @author Erwan Mahe
 */
public class BuiltinAcRevealNewPuzzle extends Action<StochasticP2PEnvironment> {

    private final Class<? extends Role> nodeRole;

    // transmission delay from clients to nodes
    private final Optional<DelaySpecification> clientsToNodesDelaySpecification;

    // delay distribution for the resolution of each puzzle by each client
    // and broadcast policy for each client
    private final HashMap<
            String,
            Pair<DelaySpecification,LedgerTransactionBroadcastFromClientToNodesPolicy>
            > clientsInformation;

    // to track the send order of transactions
    private final Optional<TransactionSendOrderTracker> sendOrderTracker;

    public BuiltinAcRevealNewPuzzle(String environment,
                                    StochasticP2PEnvironment owner,
                                    Class<? extends Role> nodeRole,
                                    Optional<DelaySpecification> clientsToNodesDelaySpecification,
                                    HashMap<String,Pair<DelaySpecification,LedgerTransactionBroadcastFromClientToNodesPolicy>> clientsInformation,
                                    Optional<TransactionSendOrderTracker> sendOrderTracker) {
        super(environment, RAbstractLedgerChannel.class, owner);
        this.nodeRole = nodeRole;
        this.clientsToNodesDelaySpecification = clientsToNodesDelaySpecification;
        this.clientsInformation = clientsInformation;
        this.sendOrderTracker = sendOrderTracker;
    }


    @Override
    public void execute() {
        List<String> shuffledClientsForFairness = new ArrayList<>(this.clientsInformation.keySet());
        Collections.shuffle(shuffledClientsForFairness);
        for (String clientName : shuffledClientsForFairness) {
            // create transaction
            PuzzleMockupTransaction tx = PuzzleMockupTransactionGenerator.getInstance().createNewTransactionForClient(clientName);
            // get policy for client
            LedgerTransactionBroadcastFromClientToNodesPolicy policy_for_client = this.clientsInformation.get(clientName).getRight();
            // create broadcast action executed upon solving the puzzle
            Action<?> broadcast_action = new AcOneTransactionBroadcast<>(
                    this.getEnvironment(),
                    this.getOwner(),
                    this.nodeRole,
                    this.clientsToNodesDelaySpecification,
                    tx,
                    policy_for_client,
                    this.sendOrderTracker
            );
            // get time to solve the puzzle
            int resolution_delay_for_client = this.clientsInformation.get(clientName).getLeft().get_concrete_delay();
            // schedule the broadcast action
            final var nextTime = this.getOwner().getSimulationTime()
                    .getCurrentTick().add(BigDecimal.valueOf(resolution_delay_for_client));
            this.getOwner().schedule(
                    new ActionActivator<>(
                            ActivationScheduleFactory.createOneTime(nextTime),
                            broadcast_action
                    ));
        }
    }

    @Override
    public <T extends Action<StochasticP2PEnvironment>> T copy() {
        return (T) new BuiltinAcRevealNewPuzzle(
                getEnvironment(),
                getOwner(),
                this.nodeRole,
                this.clientsToNodesDelaySpecification,
                this.clientsInformation,
                this.sendOrderTracker);
    }

}
