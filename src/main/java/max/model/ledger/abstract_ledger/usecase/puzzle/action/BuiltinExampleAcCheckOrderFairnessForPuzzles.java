/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.usecase.puzzle.action;


import max.core.action.Action;
import max.core.role.Role;
import max.model.ledger.abstract_ledger.usecase.puzzle.order_fairness.OrderFairnessFinalizationReference;
import max.model.ledger.abstract_ledger.usecase.puzzle.order_fairness.OrderFairnessReceptionReference;
import max.model.ledger.abstract_ledger.usecase.puzzle.order_fairness.PuzzleMockupSendOrderTracker;
import max.model.ledger.abstract_ledger.usecase.puzzle.order_fairness.properties.*;
import max.model.ledger.abstract_ledger.usecase.puzzle.order_fairness.util.OrderFairnessComputer;
import max.model.network.stochastic_adversarial_p2p.env.StochasticP2PEnvironment;
import org.apache.commons.lang3.tuple.Pair;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import max.model.ledger.abstract_ledger.role.RAbstractLedgerChannel;


/**
 * Action executed by the environment in which the ledger validators exist
 * and validate transactions related to
 * the delivery of puzzle solutions in our puzzle usecase.
 *
 * Its aim is to verify the respect of three order-fairness properties in the ledger:
 * - receive-order fairness which dictates that
 *   if a majority of validators receive a transaction X before a transaction Y
 *   then X should be delivered before Y
 * - block/wave-order fairness which dictates that
 *   if a majority of validators receive a transaction X before a transaction Y
 *   then Y should not be delivered in a block that is before the block in which X is delivered
 * - differential-order fairness is a more refined form of receive-order fairness in which
 *   the difference between the number of times X is received before Y and of times Y is received before X
 *   is used
 *
 * This action is a builtin example of an action with the aim of checking specific order fairness properties.
 *
 * @author Erwan Mahe
 */
public class BuiltinExampleAcCheckOrderFairnessForPuzzles extends Action<StochasticP2PEnvironment> {

    private Optional<String> outputCsvPath;

    private List<String> listOfClientsOnWhichToReportScore;

    private int numberOfClientsToMultiplyScore;

    private final Class<? extends Role> validatorRole;

    private final int numberOfNodesN;

    private final int byzantineThresholdF;

    private final PuzzleMockupSendOrderTracker sendOrderTracker;


    public BuiltinExampleAcCheckOrderFairnessForPuzzles(String environment,
                                                        StochasticP2PEnvironment owner,
                                                        Class<? extends Role> validatorRole,
                                                        int numberOfNodesN,
                                                        int byzantineThresholdF,
                                                        List<String> listOfClientsOnWhichToReportScore,
                                                        int numberOfClientsToMultiplyScore,
                                                        PuzzleMockupSendOrderTracker sendOrderTracker,
                                                        Optional<String> outputCsvPath) {
        super(environment, RAbstractLedgerChannel.class, owner);
        this.validatorRole = validatorRole;
        this.numberOfNodesN = numberOfNodesN;
        this.byzantineThresholdF = byzantineThresholdF;
        this.listOfClientsOnWhichToReportScore = listOfClientsOnWhichToReportScore;
        this.numberOfClientsToMultiplyScore = numberOfClientsToMultiplyScore;
        this.sendOrderTracker = sendOrderTracker;
        this.outputCsvPath = outputCsvPath;
    }



    @Override
    public void execute() {
        OrderFairnessFinalizationReference<Integer,String,Integer> finalizationReference =
                OrderFairnessFinalizationReference.fromDefaultLedgerEnvironment(
                        this.getOwner(),
                        this.getEnvironment(),
                        this.validatorRole
                );
        OrderFairnessReceptionReference<Integer,String,String> receptionReference =
                OrderFairnessReceptionReference.fromDefaultLedgerEnvironment(
                        this.getOwner(),
                        this.getEnvironment(),
                        this.validatorRole
                );
        List<Pair<String, ReceptionRelatedOrderFairnessPropertySpecification>> receptionRelatedPropertiesToCheck = new ArrayList();
        receptionRelatedPropertiesToCheck.add(
                Pair.of(
                        "receive_order_fairness",
                        new MajorityReceiveOrderFairness<>((this.numberOfNodesN / 2)+1)
                )
        );
        receptionRelatedPropertiesToCheck.add(
                Pair.of(
                        "condorcet_receive_order_fairness",
                        new CondorcetReceiveOrderFairness<>((this.numberOfNodesN / 2)+1)
                )
        );
        receptionRelatedPropertiesToCheck.add(
                Pair.of(
                        "block_order_fairness",
                        new MajorityBlockOrderFairness<>((this.numberOfNodesN / 2)+1)
                )
        );
        receptionRelatedPropertiesToCheck.add(
                Pair.of(
                        "condorcet_block_order_fairness",
                        new CondorcetBlockOrderFairness<>((this.numberOfNodesN / 2)+1)
                )
        );
        receptionRelatedPropertiesToCheck.add(
                Pair.of(
                        "differential_order_fairness",
                        new DifferentialReceiveOrderFairness<>(this.byzantineThresholdF *2)
                )
        );
        List<Pair<String, PuzzleMockupSendOrderTracker>> sendOrderPropertiesToCheck = new ArrayList<>();
        sendOrderPropertiesToCheck.add(
                Pair.of(
                        "send_order_fairness",
                        this.sendOrderTracker
                )
        );
        OrderFairnessComputer ofc = new OrderFairnessComputer(
            Optional.of(getOwner().getLogger()),
            finalizationReference,
            receptionReference,
            receptionRelatedPropertiesToCheck,
            sendOrderPropertiesToCheck,
            Optional.empty(),
            2,
            this.listOfClientsOnWhichToReportScore,
            this.numberOfClientsToMultiplyScore,
            true,
            this.outputCsvPath
        );
        ofc.execute();
    }

    @Override
    public <T extends Action<StochasticP2PEnvironment>> T copy() {
        return (T) new BuiltinExampleAcCheckOrderFairnessForPuzzles(
                getEnvironment(),
                getOwner(),
                this.validatorRole,
                this.numberOfNodesN,
                this.byzantineThresholdF,
                this.listOfClientsOnWhichToReportScore,
                this.numberOfClientsToMultiplyScore,
                this.sendOrderTracker,
                this.outputCsvPath);
    }

}
