/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.usecase.puzzle.order_fairness;

import madkit.kernel.AgentAddress;
import max.core.agent.MAXAgent;
import max.core.role.Role;
import max.model.ledger.abstract_ledger.env.LedgerValidatorContext;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupLocalLedgerState;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupTransaction;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.List;

/**
 * Information about the finalization order of the puzzle solutions.
 *
 * @author Erwan Mahe
 */
public class OrderFairnessFinalizationReference<
        PuzzleIdentifier extends Integer,   // type alias to improve code readability
        Client extends String,              // type alias to improve code readability
        BlockNumber extends Integer         // type alias to improve code readability
        > {

    // the order with which the solutions (submitted by clients) were delivered for each puzzle
    // additionally, we precise the block/wave number as part of which the transaction is delivered
    public final HashMap<PuzzleIdentifier, List<Pair<Client,BlockNumber>>> finalizationOrderPerPuzzle;

    // for each client that submits solutions, the number of times it won
    // i.e. that its submitted solution was ordered first for a given puzzle
    public final HashMap<Client,Integer> numberOfVictoriesPerClient;


    public OrderFairnessFinalizationReference(
            HashMap<PuzzleIdentifier, List<Pair<Client,BlockNumber>>> finalizationOrderPerPuzzle,
            HashMap<Client,Integer> numberOfVictoriesPerClient
    ) {
        this.finalizationOrderPerPuzzle = finalizationOrderPerPuzzle;
        this.numberOfVictoriesPerClient = numberOfVictoriesPerClient;
    }

    public static <A extends MAXAgent> OrderFairnessFinalizationReference fromDefaultLedgerEnvironment(
            A owner,
            String environment,
            Class<? extends Role> validatorRole
    ) {
        PuzzleMockupLocalLedgerState referenceLocalLedgerState = null;
        Integer maxNumKeys = 0;
        String referenceAgentName = null;
        for (AgentAddress address : owner.getAgentsWithRole(environment, validatorRole)) {
            StochasticPeerAgent agent = (StochasticPeerAgent) address.getAgent();
            LedgerValidatorContext<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState> context = (LedgerValidatorContext<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState>) agent.getContext(environment);
            // ***
            if (referenceAgentName == null) {
                referenceLocalLedgerState = context.ledgerValidatorState;
                referenceAgentName = agent.getName();
                maxNumKeys = referenceLocalLedgerState.deliveryOrderPerPuzzle.keySet().size();
            } else {
                if (context.ledgerValidatorState.deliveryOrderPerPuzzle.keySet().size() > maxNumKeys) {
                    referenceLocalLedgerState = context.ledgerValidatorState;
                    referenceAgentName = agent.getName();
                    maxNumKeys = referenceLocalLedgerState.deliveryOrderPerPuzzle.keySet().size();
                }
            }
        }
        owner.getLogger().info(
                "validator " + referenceAgentName +
                        " has the most delivered puzzle solutions which is " + maxNumKeys +
                        "\n we will use it as a reference to check delivery order"
        );
        return new OrderFairnessFinalizationReference(
                referenceLocalLedgerState.deliveryOrderPerPuzzle,
                referenceLocalLedgerState.numberOfVictoriesPerClient
        );
    }

}
