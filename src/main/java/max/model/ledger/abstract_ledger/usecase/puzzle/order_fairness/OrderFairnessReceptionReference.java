/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.usecase.puzzle.order_fairness;

import madkit.kernel.AgentAddress;
import max.core.agent.MAXAgent;
import max.core.role.Role;
import max.model.ledger.abstract_ledger.env.LedgerValidatorContext;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupLocalLedgerState;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupTransaction;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;

import java.util.HashMap;
import java.util.List;

/**
 * Information about the local reception order of each puzzle solution, for each puzzle, on each node.
 *
 * @author Erwan Mahe
 */
public class OrderFairnessReceptionReference<
        PuzzleIdentifier extends Integer,   // type alias to improve code readability
        Client extends String,              // type alias to improve code readability
        Node extends String                 // type alias to improve code readability
        > {

    public final HashMap<Node,HashMap<PuzzleIdentifier, List<Client>>> receiveOrdersPerPuzzlePerNode;

    public OrderFairnessReceptionReference(HashMap<Node,HashMap<PuzzleIdentifier, List<Client>>> receiveOrdersPerPuzzlePerNode) {
        this.receiveOrdersPerPuzzlePerNode = receiveOrdersPerPuzzlePerNode;
    }

    public static <A extends MAXAgent> OrderFairnessReceptionReference fromDefaultLedgerEnvironment(
            A owner,
            String environment,
            Class<? extends Role> validatorRole
    ) {
        HashMap<String,HashMap<Integer, List<String>>> receiveOrdersPerPuzzlePerNode = new HashMap<>();
        for (AgentAddress address : owner.getAgentsWithRole(environment, validatorRole)) {
            StochasticPeerAgent agent = (StochasticPeerAgent) address.getAgent();
            LedgerValidatorContext<PuzzleMockupTransaction, PuzzleMockupLocalLedgerState> context = (LedgerValidatorContext<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState>) agent.getContext(environment);
            // ***
            receiveOrdersPerPuzzlePerNode.put(
                    agent.getName(),
                    context.ledgerValidatorState.localReceiveOrderPerPuzzle
            );
        }
        return new OrderFairnessReceptionReference(receiveOrdersPerPuzzlePerNode);
    }

}
