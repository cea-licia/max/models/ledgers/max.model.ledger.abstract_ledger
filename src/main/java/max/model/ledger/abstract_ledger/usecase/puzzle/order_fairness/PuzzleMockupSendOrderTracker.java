/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.usecase.puzzle.order_fairness;


import max.model.ledger.abstract_ledger.action.emission.TransactionSendOrderTracker;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupTransaction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Tracks the order with which puzzle solutions are send by clients.
 *
 * @author Erwan Mahe
 */
public class PuzzleMockupSendOrderTracker implements TransactionSendOrderTracker {

    public final HashMap<Integer, List<String>> sendOrderPerPuzzle;

    public PuzzleMockupSendOrderTracker(HashMap<Integer, List<String>> sendOrderPerPuzzle) {
        this.sendOrderPerPuzzle = sendOrderPerPuzzle;
    }

    @Override
    public void track(Object transaction) {
        PuzzleMockupTransaction tx = (PuzzleMockupTransaction) transaction;
        if (tx.clientName.isPresent()) {
            String clientName = tx.clientName.get();
            if (this.sendOrderPerPuzzle.containsKey(tx.puzzleIdentifier)) {
                List<String> puzzleOrder = this.sendOrderPerPuzzle.get(tx.puzzleIdentifier);
                if (!puzzleOrder.contains(clientName)) {
                    puzzleOrder.add(clientName);
                    this.sendOrderPerPuzzle.put(tx.puzzleIdentifier,puzzleOrder);
                }
            } else {
                List<String> puzzleOrder = new ArrayList<>();
                puzzleOrder.add(clientName);
                this.sendOrderPerPuzzle.put(tx.puzzleIdentifier,puzzleOrder);
            }
        }
    }

}
