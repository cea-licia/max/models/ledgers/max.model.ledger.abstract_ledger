/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.usecase.puzzle.order_fairness;

import max.model.ledger.abstract_ledger.usecase.puzzle.order_fairness.properties.ReceptionRelatedOrderFairnessPropertySpecification;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.jgrapht.alg.connectivity.KosarajuStrongConnectivityInspector;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import java.util.*;

/**
 * All the information required to evaluate the number of violations
 * of a specific reception-related order-fairness property
 * that occurred during a simulation.
 *
 * @author Erwan Mahe
 */
public class ReceptionRelatedOrderFairnessEvaluationData<
        PuzzleIdentifier extends Integer,   // type alias to improve code readability
        Client extends String,              // type alias to improve code readability
        Node extends String,                // type alias to improve code readability
        BlockNumber extends Integer         // type alias to improve code readability
        > {

    public final HashMap<
            PuzzleIdentifier,
            Triple<
                                List<Client>,                    // the order in which clients solutions are finalized
                                HashMap<Client,BlockNumber>,     // for each client solution, in which block it is delivered
                                HashMap<Node,List<Client>>       // the reception order on each node
                                >
            > OFdata;

    private ReceptionRelatedOrderFairnessEvaluationData(
            HashMap<PuzzleIdentifier, Triple<List<Client>, HashMap<Client,BlockNumber>, HashMap<Node, List<Client>>>> oFdata
    ) {
        OFdata = oFdata;
    }

    public static <
                    PuzzleIdentifier extends Integer,
                    Client extends String,
                    Node extends String,
                    BlockNumber extends Integer
                    > ReceptionRelatedOrderFairnessEvaluationData from_references(
            OrderFairnessFinalizationReference<PuzzleIdentifier,Client,BlockNumber> finalizationReference,
            OrderFairnessReceptionReference<PuzzleIdentifier,Client,Node> receptionReference,
            Optional<Set<Node>> excludeReceptionOrderOnByzantineNodes,
            Integer minimalNumberOfFinalizedClientSolutionsToConsiderPuzzle
            ) {
        HashMap<PuzzleIdentifier, Triple<List<Client>, HashMap<Client,BlockNumber>, HashMap<Node, List<Client>>>> data = new HashMap<>();
        // we reason puzzle per puzzle
        for (PuzzleIdentifier puzzleIdentifier : finalizationReference.finalizationOrderPerPuzzle.keySet()) {
            // ***
            List<Client> finalizationOrder = new ArrayList<>();
            HashMap<Client,BlockNumber> blocksAtWhichFinalized = new HashMap<>();
            for (Pair<Client,BlockNumber> pair : finalizationReference.finalizationOrderPerPuzzle.get(puzzleIdentifier)) {
                finalizationOrder.add(pair.getLeft());
                blocksAtWhichFinalized.put(pair.getLeft(),pair.getRight());
            }
            // ***
            if (finalizationOrder.size() >= minimalNumberOfFinalizedClientSolutionsToConsiderPuzzle) {
                HashMap<Node,List<Client>> receptionOrder = new HashMap<>();
                for (Node node : receptionReference.receiveOrdersPerPuzzlePerNode.keySet()) {
                    if (excludeReceptionOrderOnByzantineNodes.isPresent() && excludeReceptionOrderOnByzantineNodes.get().contains(node)) {
                        // ignore local receive order on that node
                    } else {
                        List<Client> receiveOrderOnNode = receptionReference.receiveOrdersPerPuzzlePerNode.get(node).get(puzzleIdentifier);
                        receptionOrder.put(node,receiveOrderOnNode);
                    }
                }
                data.put(puzzleIdentifier,Triple.of(finalizationOrder,blocksAtWhichFinalized,receptionOrder));
            }
        }
        return new ReceptionRelatedOrderFairnessEvaluationData(data);
    }


    public HashMap<Pair<Client,Client>,Integer> getNumberOfHappenBeforeReceptionsForPairOfClientSolutions(
            PuzzleIdentifier puzzleIdentifier
            ) {
        HashMap<Node,List<Client>> receptionOrders = this.OFdata.get(puzzleIdentifier).getRight();
        //
        HashMap<Pair<Client,Client>,Integer> happenBeforeCount = new HashMap<>();
        for (List<Client> localReceiveOrder: receptionOrders.values()) {
            for (int i = 0; i < localReceiveOrder.size(); i++) {
                Client earlierReceivedClientSolution = localReceiveOrder.get(i);
                for (int j = i+1; j < localReceiveOrder.size(); j++) {
                    Client laterReceivedClientSolution = localReceiveOrder.get(j);
                    Pair<Client,Client> pair = Pair.of(earlierReceivedClientSolution,laterReceivedClientSolution);
                    if (happenBeforeCount.containsKey(pair)) {
                        Integer number = happenBeforeCount.get(pair);
                        number = number + 1;
                        happenBeforeCount.put(pair, number);
                    } else {
                        happenBeforeCount.put(pair, 1);
                    }
                }
            }
        }
        return happenBeforeCount;
    }

    public List<Set<Client>> getCondorcetCyclesForSolutionsOfSamePuzzle(
            PuzzleIdentifier puzzleIdentifier,
            int numberOfNodesThreshold,
            HashMap<Pair<Client,Client>,Integer> happenBeforeCount
    ) {
        DefaultDirectedGraph<Client, DefaultEdge> precedence_relation_as_graph = new DefaultDirectedGraph(DefaultEdge.class);
        List<Client> finalizationOrder =  this.OFdata.get(puzzleIdentifier).getLeft();
        for (Client client : finalizationOrder) {
            precedence_relation_as_graph.addVertex(client);
        }
        for (int i = 0; i < finalizationOrder.size(); i++) {
            Client earlierFinalizedClientSolution = finalizationOrder.get(i);
            for (int j = i+1; j < finalizationOrder.size(); j++) {
                Client laterFinalizedClientSolution = finalizationOrder.get(j);
                Integer numberOfTimesEarlierReceivedBeforeLater = happenBeforeCount.getOrDefault(
                        Pair.of(earlierFinalizedClientSolution,laterFinalizedClientSolution),
                        0
                );
                if (numberOfTimesEarlierReceivedBeforeLater >= numberOfNodesThreshold) {
                    precedence_relation_as_graph.addEdge(laterFinalizedClientSolution,earlierFinalizedClientSolution);
                }
                Integer numberOfTimesLaterReceivedBeforeEarlier = happenBeforeCount.getOrDefault(
                        Pair.of(laterFinalizedClientSolution,earlierFinalizedClientSolution),
                        0
                );
                if (numberOfTimesLaterReceivedBeforeEarlier >= numberOfNodesThreshold) {
                    precedence_relation_as_graph.addEdge(earlierFinalizedClientSolution,laterFinalizedClientSolution);
                }
            }
        }
        // Condorcet Cycles are SCC with equal or more than 2 distinct transactions
        List<Set<Client>> condorcetCycles = new ArrayList<>();
        var kosaraju = new KosarajuStrongConnectivityInspector<Client,DefaultEdge>(precedence_relation_as_graph);
        for (Set<Client> scc : kosaraju.stronglyConnectedSets()) {
            if (scc.size() >= 2) {
                condorcetCycles.add(scc);
            }
        }
        return condorcetCycles;
    }

    /**
     * Compute the number of times a specific reception-related input order-fairness property
     * is violated on this specific dataset.
     *
     * @param orderFairnessProperty : the property of interest
     */
    public Pair<Integer,Optional<Pair<Integer,Integer>>> getNumberOfViolations(ReceptionRelatedOrderFairnessPropertySpecification<Client,BlockNumber> orderFairnessProperty) {
        int violations = 0;
        int numCondorcetCycles = 0;
        int numTransactionsInCycles = 0;
        for (PuzzleIdentifier puzzleIdentifier : this.OFdata.keySet()) {
            // ***
            HashMap<Pair<Client,Client>,Integer> happenBeforeCount = this.getNumberOfHappenBeforeReceptionsForPairOfClientSolutions(puzzleIdentifier);
            // ***
            List<Set<Client>> condorcetCycles;
            if (orderFairnessProperty.getNumberOfNodesThresholdForCondorcetCyclesComputation().isPresent()) {
                int numberOfNodesThreshold = orderFairnessProperty.getNumberOfNodesThresholdForCondorcetCyclesComputation().get();
                condorcetCycles = this.getCondorcetCyclesForSolutionsOfSamePuzzle(puzzleIdentifier,numberOfNodesThreshold,happenBeforeCount);
                // ***
                numCondorcetCycles += condorcetCycles.size();
                for (Set<Client> cycle : condorcetCycles) {
                    numTransactionsInCycles += cycle.size();
                }
            } else {
                condorcetCycles = new ArrayList<>();
            }
            // ***
            List<Client> finalizationOrder =  this.OFdata.get(puzzleIdentifier).getLeft();
            HashMap<Client,BlockNumber> blockAtWhichFinalized = this.OFdata.get(puzzleIdentifier).getMiddle();
            for (int i = 0; i < finalizationOrder.size(); i++) {
                Client earlierFinalizedClientSolution = finalizationOrder.get(i);
                BlockNumber blockAtWhichEarlierFinalized = blockAtWhichFinalized.get(earlierFinalizedClientSolution);
                for (int j = i+1; j < finalizationOrder.size(); j++) {
                    Client laterFinalizedClientSolution = finalizationOrder.get(j);
                    BlockNumber blockAtWhichLaterFinalized = blockAtWhichFinalized.get(laterFinalizedClientSolution);
                    // ***
                    Integer numberOfTimesEarlierReceivedBeforeLater = happenBeforeCount.getOrDefault(
                            Pair.of(earlierFinalizedClientSolution,laterFinalizedClientSolution),
                            0
                    );
                    Integer numberOfTimesLaterReceivedBeforeEarlier = happenBeforeCount.getOrDefault(
                            Pair.of(laterFinalizedClientSolution,earlierFinalizedClientSolution),
                            0
                    );
                    // ***
                    boolean isInViolation = orderFairnessProperty.isInViolation(
                            earlierFinalizedClientSolution,
                            blockAtWhichEarlierFinalized,
                            laterFinalizedClientSolution,
                            blockAtWhichLaterFinalized,
                            numberOfTimesEarlierReceivedBeforeLater,
                            numberOfTimesLaterReceivedBeforeEarlier,
                            condorcetCycles
                    );
                    if (isInViolation) {
                        violations += 1;
                    }
                }
            }
        }
        Optional<Pair<Integer,Integer>> condorcetCyclesOpt;
        if (orderFairnessProperty.getNumberOfNodesThresholdForCondorcetCyclesComputation().isPresent()) {
            condorcetCyclesOpt = Optional.of(Pair.of(numCondorcetCycles,numTransactionsInCycles));
        } else {
            condorcetCyclesOpt = Optional.empty();
        }
        return Pair.of(violations,condorcetCyclesOpt);
    }

    /**
     * Compute the number of times send-order-fairness is violated wrt a specific send-order
     *
     * @param sendOrderTracker : the tracker that kept track of the send order
     */
    public Pair<Integer,Integer> getNumberOfSendOrderFairnessViolations(PuzzleMockupSendOrderTracker sendOrderTracker) {
        int finalization_violations = 0;
        int block_violations = 0;
        for (PuzzleIdentifier puzzleIdentifier : this.OFdata.keySet()) {
            // ***
            HashMap<Client,BlockNumber> blockAtWhichFinalized = this.OFdata.get(puzzleIdentifier).getMiddle();
            // ***
            List<Client> sendOrder = (List<Client>) sendOrderTracker.sendOrderPerPuzzle.get(puzzleIdentifier);
            List<Client> finalizationOrder =  this.OFdata.get(puzzleIdentifier).getLeft();
            for (int i = 0; i < finalizationOrder.size(); i++) {
                Client earlierFinalizedClientSolution = finalizationOrder.get(i);
                BlockNumber blockAtWhichEarlierFinalized = blockAtWhichFinalized.get(earlierFinalizedClientSolution);
                int sendIndexOfEarlierFinalized = sendOrder.indexOf(earlierFinalizedClientSolution);
                for (int j = i+1; j < finalizationOrder.size(); j++) {
                    Client laterFinalizedClientSolution = finalizationOrder.get(j);
                    BlockNumber blockAtWhichLaterFinalized = blockAtWhichFinalized.get(laterFinalizedClientSolution);
                    int sendIndexOfLaterFinalized = sendOrder.indexOf(laterFinalizedClientSolution);
                    if (sendIndexOfLaterFinalized < sendIndexOfEarlierFinalized) {
                        finalization_violations += 1;
                        boolean notInSameBlock = (Integer) blockAtWhichLaterFinalized > (Integer) blockAtWhichEarlierFinalized;
                        if (notInSameBlock) {
                            block_violations += 1;
                        }
                    }
                }
            }
        }
        return Pair.of(finalization_violations,block_violations);
    }

}
