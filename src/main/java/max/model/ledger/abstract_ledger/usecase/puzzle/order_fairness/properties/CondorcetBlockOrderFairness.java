/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.usecase.puzzle.order_fairness.properties;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * Implementation of Condorcet-(gamma)-block-order-fairness
 *
 * @author Erwan Mahe
 */
public class CondorcetBlockOrderFairness<
        Client extends String,              // type alias to improve code readability
        BlockNumber extends Integer         // type alias to improve code readability
        > implements ReceptionRelatedOrderFairnessPropertySpecification<Client,BlockNumber> {

    public final int numberOfNodesRequiredToReceiveEarlier;

    public CondorcetBlockOrderFairness(int numberOfNodesRequiredToReceiveEarlier) {
        this.numberOfNodesRequiredToReceiveEarlier = numberOfNodesRequiredToReceiveEarlier;
    }

    @Override
    public boolean isInViolation(
            Client earlierFinalizedClientSolution,
            BlockNumber blockAtWhichEarlierFinalized,
            Client laterFinalizedClientSolution,
            BlockNumber blockAtWhichLaterFinalized,
            Integer numberOfTimesEarlierReceivedBeforeLater,
            Integer numberOfTimesLaterReceivedBeforeEarlier,
            List<Set<Client>> condorcetCycles
    ) {
        // if both transactions are in a Condorcet cycle then they should be in the same block
        boolean inSameCycle = false;
        for (Set<Client> cycle : condorcetCycles) {
            if (cycle.contains(earlierFinalizedClientSolution) && cycle.contains(laterFinalizedClientSolution)) {
                inSameCycle = true;
                break;
            }
        }
        if (inSameCycle) {
            return Objects.equals(blockAtWhichLaterFinalized, blockAtWhichEarlierFinalized);
        } else {
            return false;
        }
    }


    @Override
    public Optional<Integer> getNumberOfNodesThresholdForCondorcetCyclesComputation() {
        return Optional.of(this.numberOfNodesRequiredToReceiveEarlier);
    }


}
