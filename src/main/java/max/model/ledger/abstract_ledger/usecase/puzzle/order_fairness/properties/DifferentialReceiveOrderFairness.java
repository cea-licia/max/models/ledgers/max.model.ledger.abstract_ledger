/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.usecase.puzzle.order_fairness.properties;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Implementation of differential-receive-order-fairness
 *
 * @author Erwan Mahe
 */
public class DifferentialReceiveOrderFairness<
        Client extends String,              // type alias to improve code readability
        BlockNumber extends Integer         // type alias to improve code readability
        > implements ReceptionRelatedOrderFairnessPropertySpecification<Client,BlockNumber> {

    public final int required_difference;

    public DifferentialReceiveOrderFairness(int required_difference) {
        this.required_difference = required_difference;
    }

    @Override
    public boolean isInViolation(
            Client earlierFinalizedClientSolution,
            BlockNumber blockAtWhichEarlierFinalized,
            Client laterFinalizedClientSolution,
            BlockNumber blockAtWhichLaterFinalized,
            Integer numberOfTimesEarlierReceivedBeforeLater,
            Integer numberOfTimesLaterReceivedBeforeEarlier,
            List<Set<Client>> condorcetCycles
    ) {
        return numberOfTimesLaterReceivedBeforeEarlier > numberOfTimesEarlierReceivedBeforeLater + this.required_difference;
    }

    @Override
    public Optional<Integer> getNumberOfNodesThresholdForCondorcetCyclesComputation() {
        return Optional.empty();
    }

}
