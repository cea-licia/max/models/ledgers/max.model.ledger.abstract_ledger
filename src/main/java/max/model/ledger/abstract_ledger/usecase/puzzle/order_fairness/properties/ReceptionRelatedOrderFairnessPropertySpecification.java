/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.usecase.puzzle.order_fairness.properties;


import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Abstract interface of a reception-related order-fairness property.
 * It provides a unique method that checks, for a pair of transactions, if the property is violated
 * according to information about
 * their relative finalization order and reception order.
 *
 * @author Erwan Mahe
 */
public interface ReceptionRelatedOrderFairnessPropertySpecification<
        Client extends String,              // type alias to improve code readability
        BlockNumber extends Integer         // type alias to improve code readability
        > {

    /**
     * If, when reasoning on a pair of transactions,
     * this property requires knowledge of whether these two transactions belong to the same Condorcet cycle
     * then this method returns the threshold parameter with which to compute these cycles.
     * **/
    Optional<Integer> getNumberOfNodesThresholdForCondorcetCyclesComputation();

    /**
     * Tells whether, for two transactions:
     * - the *earlierFinalized* transaction
     * - and the *laterFinalized* transaction
     * The given property is violated.
     * This computation can use additional information from:
     * - the block in which the first transaction is finalized
     * - the block in which the second transaction is finalized
     * - the list of pertinent Condorcet cycles (i.e. those in which the two transactions are susceptible to belong)
     * **/
    boolean isInViolation(
            Client earlierFinalizedClientSolution,
            BlockNumber blockAtWhichEarlierFinalized,
            Client laterFinalizedClientSolution,
            BlockNumber blockAtWhichLaterFinalized,
            Integer numberOfTimesEarlierReceivedBeforeLater,
            Integer numberOfTimesLaterReceivedBeforeEarlier,
            List<Set<Client>> condorcetCycles
    );


}
