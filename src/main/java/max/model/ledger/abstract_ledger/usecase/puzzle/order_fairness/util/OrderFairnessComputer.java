/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.usecase.puzzle.order_fairness.util;

import madkit.kernel.AgentLogger;
import max.model.ledger.abstract_ledger.usecase.puzzle.order_fairness.OrderFairnessFinalizationReference;
import max.model.ledger.abstract_ledger.usecase.puzzle.order_fairness.OrderFairnessReceptionReference;
import max.model.ledger.abstract_ledger.usecase.puzzle.order_fairness.PuzzleMockupSendOrderTracker;
import max.model.ledger.abstract_ledger.usecase.puzzle.order_fairness.ReceptionRelatedOrderFairnessEvaluationData;
import max.model.ledger.abstract_ledger.usecase.puzzle.order_fairness.properties.ReceptionRelatedOrderFairnessPropertySpecification;

import org.apache.commons.lang3.tuple.Pair;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;


/**
 * Utility class to compute the number of violations:
 * - of a number of reception-related order-fairness properties
 * - and a number of emission-related order-fairness properties
 * - with regards to a certain finalization order
 * - and a certain reception order
 * - also computes the client-fairness scores of specific clients
 *
 * It may log all these metrics on the console and also produce a CSV file
 *
 * @author Erwan Mahe
 */
public class OrderFairnessComputer {

    private final Optional<AgentLogger> logger;

    private final OrderFairnessFinalizationReference<Integer,String,Integer> finalizationReference;

    private final OrderFairnessReceptionReference<Integer,String,String> receptionReference;

    private final List<Pair<String, ReceptionRelatedOrderFairnessPropertySpecification>> receptionRelatedPropertiesToCheck;

    private final List<Pair<String,PuzzleMockupSendOrderTracker>> sendOrderPropertiesToCheck;

    private final Optional<Set<String>> excludeReceptionOrderOnByzantineNodes;
    private final int minimalNumberOfFinalizedClientSolutionsToConsiderPuzzle;

    private final List<String> listOfClientsOnWhichToReportScore;

    private final int numberOfClientsToMultiplyScore;

    private final boolean printNumberOfPuzzlesSolved;

    private final Optional<String> outputCsvPath;

    public OrderFairnessComputer(
            Optional<AgentLogger> logger,
            OrderFairnessFinalizationReference<Integer, String, Integer> finalizationReference,
            OrderFairnessReceptionReference<Integer, String, String> receptionReference,
            List<Pair<String, ReceptionRelatedOrderFairnessPropertySpecification>> receptionRelatedPropertiesToCheck,
            List<Pair<String, PuzzleMockupSendOrderTracker>> sendOrderPropertiesToCheck,
            Optional<Set<String>> excludeReceptionOrderOnByzantineNodes,
            int minimalNumberOfFinalizedClientSolutionsToConsiderPuzzle,
            List<String> listOfClientsOnWhichToReportScore,
            int numberOfClientsToMultiplyScore,
            boolean printNumberOfPuzzlesSolved,
            Optional<String> outputCsvPath
    ) {
        this.logger = logger;
        this.finalizationReference = finalizationReference;
        this.receptionReference = receptionReference;
        this.receptionRelatedPropertiesToCheck = receptionRelatedPropertiesToCheck;
        this.sendOrderPropertiesToCheck = sendOrderPropertiesToCheck;
        this.excludeReceptionOrderOnByzantineNodes = excludeReceptionOrderOnByzantineNodes;
        this.minimalNumberOfFinalizedClientSolutionsToConsiderPuzzle = minimalNumberOfFinalizedClientSolutionsToConsiderPuzzle;
        this.listOfClientsOnWhichToReportScore = listOfClientsOnWhichToReportScore;
        this.numberOfClientsToMultiplyScore = numberOfClientsToMultiplyScore;
        this.printNumberOfPuzzlesSolved = printNumberOfPuzzlesSolved;
        this.outputCsvPath = outputCsvPath;
    }

    public void execute() {
        ReceptionRelatedOrderFairnessEvaluationData<Integer,String,String,Integer> data =
                ReceptionRelatedOrderFairnessEvaluationData.from_references(
                        finalizationReference,
                        receptionReference,
                        excludeReceptionOrderOnByzantineNodes,
                        minimalNumberOfFinalizedClientSolutionsToConsiderPuzzle
                );
        // ***
        int numberOfPuzzlesSolved = finalizationReference.finalizationOrderPerPuzzle.keySet().size();
        List<String> csvHeader = new ArrayList<>();
        List<String> csvLine = new ArrayList<>();
        // collect client-fairness i.e., in this usecase,
        // the likelihood of each client to win puzzles
        // (over the repeated instances during the simulation)
        // this is summarized via a score given to each client
        // this scores corresponds to : %w * n
        // where %w corresponds to the percentage of WON puzzles for a given client
        // and n the total number of clients
        // if the simulation is "client-fair" then every client has 1/n changes of winning every puzzle
        // hence, the simulation is "client-fair" if the scores of all clients converge towards 1
        if (this.printNumberOfPuzzlesSolved) {
            csvHeader.add("numberOfPuzzlesSolved");
            csvLine.add( String.valueOf(numberOfPuzzlesSolved) );
        }
        if (numberOfPuzzlesSolved > 0) {
            // for each client, write the score in a CSV cell
            for (String clientName : this.listOfClientsOnWhichToReportScore) {
                csvHeader.add( "client_fairness_score_" + clientName);
                if (finalizationReference.numberOfVictoriesPerClient.containsKey(clientName)) {
                    Integer numVictories = finalizationReference.numberOfVictoriesPerClient.get(clientName);
                    float prop = ((float) numVictories )/ ( (float) numberOfPuzzlesSolved );
                    float score = prop * ( (float) this.numberOfClientsToMultiplyScore);
                    csvLine.add( String.valueOf(score) );
                    logger.ifPresent(
                            x -> x.info("client : " + clientName + " has a client fairness score of : " + String.valueOf(score))
                    );
                } else {
                    csvLine.add( "0.0" );
                }
            }
            // then write the number of order fairness violations for each reception-related property
            for (Pair<String, ReceptionRelatedOrderFairnessPropertySpecification> pair : this.receptionRelatedPropertiesToCheck) {
                String propertyDescription = pair.getLeft();
                ReceptionRelatedOrderFairnessPropertySpecification propertySpecification = pair.getRight();
                Pair<Integer,Optional<Pair<Integer,Integer>>> propComp = data.getNumberOfViolations(propertySpecification);
                int numberOfTimesViolated = propComp.getLeft();
                if (propComp.getRight().isPresent()) {
                    int numCycles = propComp.getRight().get().getLeft();
                    int numTransactionsInCycles = propComp.getRight().get().getRight();
                    logger.ifPresent(
                            x -> x.info(String.valueOf(numCycles) + " Condorcet cycles found when computing violations of " + propertyDescription)
                    );
                    logger.ifPresent(
                            x -> x.info(String.valueOf(numTransactionsInCycles) + " transactions stuck in Condorcet cycles found when computing violations of " + propertyDescription)
                    );
                    csvHeader.add("condorcet_cycles_for" + propertyDescription);
                    csvLine.add( String.valueOf(numCycles) );
                    csvHeader.add("transactions_in_condorcet_cycles_for" + propertyDescription);
                    csvLine.add( String.valueOf(numTransactionsInCycles) );
                }
                logger.ifPresent(
                        x -> x.info(propertyDescription + " for puzzle solutions has been violated : " + String.valueOf(numberOfTimesViolated) + " times")
                );
                csvHeader.add(propertyDescription);
                csvLine.add( String.valueOf(numberOfTimesViolated) );
            }
            // then write the number of order fairness violations for each emission-related property
            for (Pair<String, PuzzleMockupSendOrderTracker> sendOrderDescriptor : this.sendOrderPropertiesToCheck) {
                String sendOrderDescription = sendOrderDescriptor.getLeft();
                PuzzleMockupSendOrderTracker sendOrderTracker = sendOrderDescriptor.getRight();
                Pair<Integer,Integer> numberOfTimesViolated = data.getNumberOfSendOrderFairnessViolations(sendOrderTracker);
                {
                    String propertyDescription = "finalization_order_fairness_" + sendOrderDescription;
                    int wav_violations = numberOfTimesViolated.getLeft();
                    logger.ifPresent(
                            x -> x.info(
                                    propertyDescription +
                                            " for puzzle solutions has been violated : " +
                                            String.valueOf(wav_violations) +
                                            " times"
                            )
                    );
                    csvHeader.add(propertyDescription);
                    csvLine.add( String.valueOf(wav_violations) );
                }
                {
                    String propertyDescription = "wave_order_fairness_" + sendOrderDescription;
                    int wav_violations = numberOfTimesViolated.getRight();
                    logger.ifPresent(
                            x -> x.info(
                                    propertyDescription +
                                            " for puzzle solutions has been violated : " +
                                            String.valueOf(wav_violations) +
                                            " times"
                            )
                    );
                    csvHeader.add(propertyDescription);
                    csvLine.add( String.valueOf(wav_violations) );
                }


            }
            // ***
            // Print results as a line in a CSV file
            if (this.outputCsvPath.isPresent()) {
                String outputCsvFilePath = this.outputCsvPath.get();
                try {
                    FileWriter fw = new FileWriter(outputCsvFilePath, false);
                    BufferedWriter bw = new BufferedWriter(fw);
                    bw.write( String.join(";", csvHeader) + "\n" );
                    bw.write( String.join(";", csvLine) + "\n" );
                    bw.newLine();
                    bw.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

}
