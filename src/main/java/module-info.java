open module max.model.ledger.abstract_ledger {
    // Exported packages
    exports max.model.ledger.abstract_ledger.action.check;
    exports max.model.ledger.abstract_ledger.action.config;
    exports max.model.ledger.abstract_ledger.action.emission;
    exports max.model.ledger.abstract_ledger.action.transact;
    exports max.model.ledger.abstract_ledger.approval;
    exports max.model.ledger.abstract_ledger.env;
    exports max.model.ledger.abstract_ledger.role;
    exports max.model.ledger.abstract_ledger.state;
    exports max.model.ledger.abstract_ledger.usecase.puzzle;
    exports max.model.ledger.abstract_ledger.usecase.puzzle.action;
    exports max.model.ledger.abstract_ledger.usecase.puzzle.order_fairness;
    exports max.model.ledger.abstract_ledger.usecase.puzzle.order_fairness.properties;
    exports max.model.ledger.abstract_ledger.usecase.puzzle.order_fairness.util;

    // Dependencies
    requires java.logging;
    requires java.desktop;
    requires org.apache.commons.lang3;
    requires org.jgrapht.core;

    requires madkit;      // Automatic

    requires max.core;
    requires max.datatype.com;
    requires max.model.network.stochastic_adversarial_p2p;

    // Optional dependency (required for tests)
    requires static org.junit.jupiter.api;

}