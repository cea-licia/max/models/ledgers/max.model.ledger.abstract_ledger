/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.abstract_ledger.env;

import static max.core.MAXParameters.clearParameters;
import static max.core.MAXParameters.setParameter;
import static max.core.test.TestMain.launchTester;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import max.core.action.ACTakeRole;
import max.core.action.Plan;
import max.core.agent.ExperimenterAgent;
import max.core.agent.MAXAgent;
import max.core.scheduling.ActionActivator;
import max.core.scheduling.ActivationScheduleFactory;
import max.model.ledger.abstract_ledger.role.RLedgerValidator;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupLocalLedgerState;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupTransaction;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.io.TempDir;



/**
 * Basic Test in which we simply instantiate 2 peers in an environment.
 *
 * @author Erwan Mahe
 */
public class BasicInstanciationTest {

  @BeforeEach
  public void before(@TempDir Path tempDir) {
    clearParameters();

    setParameter("MAX_CORE_SIMULATION_STEP", BigDecimal.ONE.toString());
    setParameter("MAX_CORE_UI_MODE", "Silent");
    setParameter("MAX_CORE_RESULTS_FOLDER_NAME", tempDir.toString());
  }

  @Test
  public void test(TestInfo testInfo) throws Throwable {
    final List<List<?>> storage = new ArrayList<>();
    final var tester =
        new ExperimenterAgent() {

          private AbstractLedgerEnvironment<PuzzleMockupTransaction, PuzzleMockupLocalLedgerState> env;
            private StochasticPeerAgent peer1;
            private StochasticPeerAgent peer2;

          @Override
          protected List<MAXAgent> setupScenario() {
            final List<MAXAgent> agents = new ArrayList<>();

            env = new AbstractLedgerEnvironment<PuzzleMockupTransaction, PuzzleMockupLocalLedgerState>();
            agents.add(env);

            peer1 = new StochasticPeerAgent(createPeerPlan(env.getName()));
            agents.add(peer1);

            peer2 = new StochasticPeerAgent(createPeerPlan(env.getName()));
            agents.add(peer2);

            return agents;
          }
        };
    launchTester(tester, 10, testInfo);
  }

  private Plan<StochasticPeerAgent> createPeerPlan(String envName) {
    return new Plan<>() {
      @Override
      public List<ActionActivator<StochasticPeerAgent>> getInitialPlan() {
        return List.of(
            new ActionActivator<>(
                ActivationScheduleFactory.ONE_SHOT_AT_STARTUP,
                new ACTakeRole<>(envName, RLedgerValidator.class, null))
        );
      }
    };
  }

}
